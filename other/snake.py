import pygame
import time
import random

pygame.init()

white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)

width = 800
height = 800

screen = pygame.display.set_mode((width, height))

clock = pygame.time.Clock()

block_size = 10
snake_speed = 15

font_style = pygame.font.SysFont("bahnschrift", 35)
score_font = pygame.font.SysFont("comicsansms", 35)

# apple_type = [
#     [green, 10],
#     [red, -5]
# ]


def Your_score(score):
    value = score_font.render("Your Score: " + str(score), True, yellow)
    screen.blit(value, [0, 0])


def our_snake(block_size, snake_list):
    for x in snake_list:
        pygame.draw.rect(screen, black, [x[0], x[1], block_size, block_size])


def message(msg, color):
    mesg = font_style.render(msg, True, color)
    screen.blit(mesg, [width / 6, height / 3])


def gameLoop():
    game_over = False
    game_close = False

    x1 = width / 2
    y1 = height / 2

    x1_change = 0
    y1_change = 0

    snake_List = []
    Length_of_snake = 1

    foodx = round(random.randrange(0, width - block_size) / block_size) * block_size
    foody = round(random.randrange(0, height - block_size) / block_size) * block_size

    count = 0
    color = green
    while not game_over:
        while game_close == True:
            screen.fill(blue)
            message("You Lost! Press C-Play Again or Q-Quit", red)
            Your_score(Length_of_snake - 1)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        game_over = True
                        game_close = False
                    if event.key == pygame.K_c:
                        gameLoop()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x1_change = -block_size
                    y1_change = 0
                elif event.key == pygame.K_RIGHT:
                    x1_change = block_size
                    y1_change = 0
                elif event.key == pygame.K_UP:
                    y1_change = -block_size
                    x1_change = 0
                elif event.key == pygame.K_DOWN:
                    y1_change = block_size
                    x1_change = 0

        # DO NOT PASS THROUGH THE SIDE-WALL
        # if x1 >= width or x1 < 0 or y1 >= height or y1 < 0:
        # game_close = True

        if x1 > width:
            x1 = 0
        if y1 > height:
            y1 = 0
        if x1 < 0:
            x1 = width
        if y1 < 0:
            y1 = height

        x1 += x1_change
        y1 += y1_change
        screen.fill(blue)
        pygame.draw.rect(screen, color, [foodx, foody, block_size, block_size])
        snake_Head = []
        snake_Head.append(x1)
        snake_Head.append(y1)
        snake_List.append(snake_Head)
        if len(snake_List) > Length_of_snake:
            del snake_List[0]

        for x in snake_List[:-1]:
            if x == snake_Head:
                # game_close = True
                snake_List = snake_List[:-10]
                Length_of_snake -= 10

        our_snake(block_size, snake_List)
        Your_score(Length_of_snake - 1)

        pygame.display.update()

        if x1 == foodx and y1 == foody:
            foodx = (
                round(random.randrange(0, width - block_size) / block_size) * block_size
            )
            foody = (
                round(random.randrange(0, height - block_size) / block_size)
                * block_size
            )

            # ALEATORY APPLE TYPE
            # apple = apple_type[random.randrange(0, 2)]
            # color = apple[0]
            # point = apple[1]

            # LONG SNAKE FROM START
            # if Length_of_snake < 300:
            #     Length_of_snake = 600

            Length_of_snake += 10

        clock.tick(snake_speed)

    pygame.quit()
    quit()


gameLoop()
