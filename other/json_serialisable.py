from faker import Faker
import io, json, random

fake = Faker("fr_FR")

date = fake.date_between(start_date="+30d", end_date="+35d")
print("date :", date)

strftime = json.dumps(date.strftime("%d/%m/%Y"))
print("get a string :", strftime)
while True:
    try:
        json.dumps(date)
        break
    except TypeError:
        print("Object is not JSON serialisable")
        break
