import threading
import time
from random import randint


def flag():
    time.sleep(3)
    event.set()
    print("start the run")
    time.sleep(5)
    print("the run is finished")
    event.clear()


def start_operation():
    event.wait()
    while event.is_set():
        x = randint(0, 30)
        time.sleep(0.1)
        if x == 25:
            print("you find the answer !")
    print("event is cleared")


event = threading.Event()
t1 = threading.Thread(target=flag)
t2 = threading.Thread(target=start_operation)

t1.start()
t2.start()
