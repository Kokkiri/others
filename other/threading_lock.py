import threading
from time import sleep
import inspect


class Thread(threading.Thread):
    def __init__(self, t, *args):
        threading.Thread.__init__(self, target=t, args=args)
        self.start()


count = 0
lock = threading.Lock()


def increment():
    global count
    caller = inspect.getouterframes(inspect.currentframe())[1][3]
    print("Inside %s()" % caller)
    print("Acquiring lock")
    with lock:
        print("Lock Acquired")
        count += 1
        sleep(2)


def bye():
    while count < 5:
        increment()


def hello_there():
    while count < 5:
        increment()


def main():
    hello = Thread(hello_there)
    goodbye = Thread(bye)


if __name__ == "__main__":
    main()
