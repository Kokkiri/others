# CONVERTIR UN NOMBRE EN CHIFFRE ROMAIN

# I, V, X, L, C, D, M

res = ""
user_input = int(input("choisissez un nombre entre 1 et 3999 :"))
while not (user_input >= 1 and user_input <= 3999):
    user_input = int(input("choisissez un nombre entre 1 et 3999 :"))
original_user_input = user_input

while user_input >= 1000:
    res += "M"
    user_input -= 1000
if user_input >= 900:
    res += "CM"
    user_input -= 900
if user_input >= 500:
    res += "D"
    user_input -= 500
if user_input >= 400:
    res += "CD"
    user_input -= 400
while user_input >= 100:
    res += "C"
    user_input -= 100
if user_input >= 90:
    res += "XC"
    user_input -= 90
if user_input >= 50:
    res += "L"
    user_input -= 50
if user_input >= 40:
    res += "XL"
    user_input -= 40
while user_input >= 10:
    res += "X"
    user_input -= 10
if user_input >= 9:
    res += "IX"
    user_input -= 9
if user_input >= 5:
    res += "V"
    user_input -= 5
if user_input >= 4:
    res += "IV"
    user_input -= 4
while user_input >= 3:
    res += "I"
    user_input -= 1

print(f"{original_user_input} est égale à {res} en nombre romain")
