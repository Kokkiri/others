from random import randint


def listAleaInt(n, a, b):
    list = []
    for i in range(n):
        list.append(randint(a, b))
    return list


def call_list():
    n = int(input("n"))
    a = int(input("a"))
    b = int(input("b"))
    list = listAleaInt(n, a, b)
    print("liste avant échange: ", list)
    i_min = list.index(min(list))
    print("indice de la valeur minimum: ", i_min)
    first = list[0]
    list[0] = list[i_min]
    list[i_min] = first
    print("liste après échange: ", list)


call_list()
