from functools import reduce

panier = [
    {"name": "chaussures", "price": 55, "quantity": 1},
    {"name": "livre", "price": 10, "quantity": 2},
    {"name": "cookie", "price": 5, "quantity": 3},
]


def cumul(acc, item):
    acc["total_price"] += item["price"] * item["quantity"]
    acc["total_item"] += item["quantity"]
    return acc


total = reduce(cumul, panier, {"total_price": 0, "total_item": 0})
print(total)
