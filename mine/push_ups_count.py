#!/bin/python3

# base on the prog termdown by:
# Torsten Rehn <torsten@rehn.email>
# Peter Hofmann <scm@uninformativ.de>
# Bennett Piater <bennett at piater dot name>

from time import sleep
from datetime import datetime, timedelta
from playsound import playsound
from pyfiglet import Figlet
from threading import Event, Lock, Thread
import curses
from queue import Queue, Empty


font = Figlet(font="univers")

INPUT_PAUSE = 1
INPUT_EXIT = 2


def setup(stdscr):
    # curses
    curses.use_default_colors()
    curses.init_pair(1, 94, -1)
    try:
        curses.curs_set(False)
    except curses.error:
        # fails on some terminals
        pass
    stdscr.timeout(0)

    # prepare input thread mechanisms
    curses_lock = Lock()
    input_queue = Queue()
    quit_event = Event()
    return (curses_lock, input_queue, quit_event)


def input_thread_body(stdscr, input_queue, quit_event, curses_lock):
    while not quit_event.is_set():
        try:
            with curses_lock:
                key = stdscr.getkey()
        except:
            key = None
        if key in ("q", "Q"):
            input_queue.put(INPUT_EXIT)
        elif key == " ":
            input_queue.put(INPUT_PAUSE)


def sound_1():
    playsound("/usr/share/sounds/sound-icons/percussion-12.wav")


def sound_2():
    playsound("/usr/share/sounds/sound-icons/percussion-10.wav")


def bell_tower_every_minutes(minute):
    for i in range(int(str(minute).zfill(2)[0])):
        th2 = Thread(target=sound_2)
        th2.start()
        sleep(0.3)
    sleep(0.3)
    playsound("/usr/share/sounds/sound-icons/beginning-of-line")
    for i in range(int(str(minute).zfill(2)[1])):
        th2 = Thread(target=sound_2)
        th2.start()
        sleep(0.3)


def count(stdscr):

    curses_lock, input_queue, quit_event = setup(stdscr)

    input_thread = Thread(
        args=(stdscr, input_queue, quit_event, curses_lock),
        target=input_thread_body,
    )
    input_thread.start()

    seconde = 0
    minute = seconde // 60
    pause_start = None
    sync_start = datetime.now()
    seconds_elapsed = 0

    try:
        while seconde < 3600:
            sleep_target = sync_start + timedelta(seconds=seconds_elapsed + 1)
            now = datetime.now()

            text = f"{str(minute).zfill(2)} : {str(seconde).zfill(2)}"
            if sleep_target > now:
                try:
                    input_action = input_queue.get(
                        True, (sleep_target - now).total_seconds()
                    )
                except Empty:
                    input_action = None
                if input_action == INPUT_PAUSE:
                    pause_start = datetime.now()
                    print("start pause", pause_start)
                    print("sanc_start", sync_start)
                    with curses_lock:
                        stdscr.attron(curses.color_pair(1))
                        stdscr.addstr(0, 0, font.renderText(text))
                        stdscr.attroff(curses.color_pair(1))
                    input_action = input_queue.get()
                    if input_action == INPUT_PAUSE:
                        sync_start += datetime.now() - pause_start
                        pause_start = None
                if input_action == INPUT_EXIT:
                    break

                stdscr.clear()
                stdscr.addstr(0, 0, font.renderText(str(text)))

                if seconde == 30:
                    th1 = Thread(target=sound_1)
                    th1.start()
                if seconde == 0:
                    th3 = Thread(target=bell_tower_every_minutes, args=(minute,))
                    th3.start()

                if seconde == 59:
                    minute += 1
                    seconde = -1
                seconde += 1
            seconds_elapsed = int((datetime.now() - sync_start).total_seconds())

    finally:
        quit_event.set()
        input_thread.join()


if __name__ == "__main__":
    curses.wrapper(count)
