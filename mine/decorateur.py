def deco_my_func(func):
    def wrapper():
        print("__________")
        return func()

    return wrapper


@deco_my_func
def frout():
    print("frout")


@deco_my_func
def frotita():
    print("frotita")


frout()
frotita()
