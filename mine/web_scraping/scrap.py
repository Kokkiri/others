import requests
from bs4 import BeautifulSoup

######################## WEB SCRAPING #########################

url = "https://www.linuxjobs.fr/"
html = requests.get(url).text
html_page = BeautifulSoup(html, "html.parser")
div = html_page.find("div", {"class": "col-md-9"})
titles = div.find_all("h3")


def get_title():
    for title in titles:
        if "Programmeur" in title.get_text():
            return title


title = get_title()

# GET NEXT ELEMENT OF AN ELEMENT IN DOM
jobs = title.next_sibling.next_sibling


def get_jobs():
    job_list = []
    for job in jobs.strings:
        job_list.append(job)
    for job in job_list:
        if job != "\n":
            print(job)


# jobs = get_jobs()

################################ DOWNLOAD ###################################

download_url = "https://www.linuxjobs.fr/categories/1/postes-programmeur/rss"
req = requests.get(download_url)
filename = req.url[download_url.rfind("/") + 1 :]

with open(filename, "wb") as f:
    for chunk in req.iter_content(chunk_size=8192):
        if chunk:
            f.write(chunk)
