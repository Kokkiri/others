// GET ELEMENTS FROM XML FILES AND RETURN IT INTO HTML PAGE

document.addEventListener('DOMContentLoaded', () => {
    let url = 'rss'
    fetch(url)
    .then(resp => resp.text())
    .then(data => {
        let parser = new DOMParser()
        let xml = parser.parseFromString(data, "application/xml")
        document.getElementById('root').textContent = data
        console.log(xml)
        job_list(xml)
    })
})

function job_list(xml_doc) {
    let list = document.getElementById('lili')
    let houses = xml_doc.getElementsByTagName('pubDate')
    for (let i = 0; i < houses.length; i++) {
        let li = document.createElement('li')
        let house = houses[i].firstChild.nodeValue
        li.textContent = house
        list.appendChild(li)
    }
}