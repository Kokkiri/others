#!/usr/bin/python3

import argparse
from termcolor import colored
import json

with open("comptes.json") as file:
    data = json.load(file)
    solde = data.get("solde")

    print(f"\tdernier solde\t|\t{solde}")

    parser = argparse.ArgumentParser(
        description="calcule le solde à chaque fin de cours de dessin"
    )
    parser.add_argument("--prix", "-p", type=float, help="prix du verre")
    parser.add_argument("--money", "-m", type=float, help="money rembourser")
    args = parser.parse_args()

    if args.prix == None:
        args.prix = 0
    if args.money == None:
        args.money = 0

    print(f"\tprix du verre\t|\t{args.prix}")
    print(f"\targent versé\t|\t{args.money}")

    solde -= args.prix
    solde += args.money

    solde = round(solde, 2)

    if solde < 0:
        print(colored(f"\tsolde en cours\t|\t{solde}", "red"))
    elif solde > 0:
        print(colored(f"\tsolde en cours\t|\t{solde}", "green"))
    else:
        print(f"\tsolde en cours\t|\t{solde}")

    data["solde"] = solde

with open("./comptes.json", "w", encoding="utf-8") as f:
    f.write(json.dumps(data, ensure_ascii=False))
