import requests
from bs4 import BeautifulSoup
from requests_html import HTMLSession
from selenium import webdriver

url = "https://soundcloud.com/search?q=hiroshi%20yoshimura"
# url = 'https://dyma.fr/developer/list/chapters/core/5f99eb72545c3568e6800990/lesson/docker/5f99ec25545c3568e6800996/1/4= '


# USAGE DE SELENIUM #####################################

# driver = webdriver.PhantomJS()
# driver.get(url)
# page = BeautifulSoup(driver.page_source) #page_source fetches page after rendering is complete
# driver.save_screenshot('screen.png') # save a screenshot to disk

# driver.quit()

# print(dir(webdriver))

# USAGE DE REQUESTS_HTML ################################

session = HTMLSession()
resp = session.get(url)
soup = BeautifulSoup(resp.html.html, "lxml")

print(soup)
