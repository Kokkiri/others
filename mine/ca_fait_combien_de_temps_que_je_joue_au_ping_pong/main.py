#!/usr/bin/env python3

from datetime import datetime

start_date = datetime(2024, 6, 17)
duration = datetime.now() - start_date

# annees = duration.days // 365
mois = (duration.days % 365) // 30
jours = duration.days % 30


print(f"Ça fait {mois} mois et {jours} jours")
