#!/bin/python3

import os
import shutil
import re

# list current files and folders
files_list = [name for name in os.listdir(".")]

# replace some character
files_to_lowercase = [files.lower() for files in files_list]
replace_space_by_underscore = [re.sub(r"[ \-']", "_", f) for f in files_to_lowercase]
replace_multi_underscore_by_underscore = [
    re.sub(r"[__]+", "_", f) for f in replace_space_by_underscore
]
iteration_c = [f.replace("ç", "c") for f in replace_multi_underscore_by_underscore]
iteration_e = [f.translate({ord(c): "e" for c in "éèê"}) for f in iteration_c]
iteration_a = [f.translate({ord(c): "a" for c in "àâ"}) for f in iteration_e]

# rename all files and folders
for i in range(len(files_list)):
    shutil.move(files_list[i], iteration_a[i])
