from random import random
from grid import Grid
from cell import Cell
from constants import (
    GRID_SIZE,
    NUM_ALLY,
    NUM_ENEMY,
    NUM_MURS,
    NUM_MINES,
    ATTACK_POINT,
    LIFE_ALLY,
    LIFE_ENEMY,
)


ALLY = [Cell(_type="ally", value=LIFE_ALLY) for i in range(NUM_ALLY)]
ENEMY = [Cell(_type="enemy", value=LIFE_ENEMY) for i in range(NUM_ENEMY)]
WALLS = [Cell(_type="wall") for i in range(NUM_MURS)]
MINES = [Cell(_type="mine") for i in range(NUM_MINES)]
pieces = ALLY + ENEMY + WALLS + MINES
MAIN_GRID = Grid(GRID_SIZE, ALLY, ENEMY)
MAIN_GRID.set_positions(pieces=pieces)
TEAMS = {"ally": [ALLY, 0], "enemy": [ENEMY, 0]}


def kill(unit):
    _type = unit.get_type()
    # if target is dead, it's removed from his team and the cell is initialized to a default cell
    TEAMS[_type][0].remove(unit)
    # even if the target is remove from the list and the cell have change color on the map, we need to reset type attribute
    unit.reset()
    # if the dead opponent is the last of the list of his team, initialize his index to 0 to except IndexError
    if len(TEAMS[_type][0]) == TEAMS[_type][1]:
        TEAMS[_type][1] = 0


def turn_by_turn():
    """while this is not the end, every cells of each team move and attack"""

    team_and_index = [TEAMS["ally"], TEAMS["enemy"]]
    target = None
    no_way = []
    while True:

        for index in range(len(team_and_index)):

            current_team = team_and_index[index][0]
            current_index = team_and_index[index][1]
            current_cell = current_team[current_index]

            current_team_opponent = team_and_index[index - 1][0]

            # get position of each cells on the grid, and initialize "previous" attribute of cells
            MAIN_GRID.get_all_neighbors()

            # get the result after moving cell (result can be None, the target to reach, or cells to be removed cause of mines)
            msg = MAIN_GRID.move_piece(current_cell, team_and_index[index - 1][0])

            if msg != None:
                # if a bomb is drop remove all dead_unit from their own teams
                if msg[0] == "bomb":
                    for cell in msg[1]:
                        kill(cell)

                # if current_cell have explode before reaching his target
                elif msg[0] == "mine":
                    kill(msg[1])

                    MAIN_GRID.refresh()
                    # if the list of the team is empty, battle is finished, print the score
                    if current_team == []:
                        MAIN_GRID.refresh()
                        return

                # if there's a reachable target
                elif msg[0] == "reach":
                    no_way = []
                    # if the target is dead
                    if current_cell.attack(msg[1], ATTACK_POINT) <= 0:
                        kill(msg[1])

                        MAIN_GRID.refresh()
                        # if the list of the team is empty, battle is finished, print the score
                        if current_team_opponent == []:
                            MAIN_GRID.refresh()
                            return
                    else:
                        MAIN_GRID.refresh()

            # if both teams are alive but there's no more solution, live the program
            else:
                no_way.append("no_way")
                # if the number of negative consecutive response is equal to cells that remains, there's no more solution. get the score
                if len(no_way) == len(current_team + current_team_opponent):
                    return

            # increase index of the current team to let play each cell of a team one after an other
            team_and_index[index][1] += 1
            if team_and_index[index][1] > len(current_team) - 1:
                team_and_index[index][1] = 0


def main():
    MAIN_GRID.refresh()
    turn_by_turn()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        quit()
