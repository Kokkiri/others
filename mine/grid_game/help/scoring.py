SIZE = 30
ALLY = 100
ENEMY = 100

# front
NATIF = '\033[0m'
BLACK = '\033[30m'

# back
BACK_RED = '\033[48;5;196m'
BACK_BLUE = '\033[48;5;27m'
BACK_GREEN_UP = '\033[48;5;46m'

TOTAL = ALLY + ENEMY
RATIO = SIZE / TOTAL * 2

scoring = BACK_BLUE + BLACK + (str(ALLY) + str(' '*round(ALLY*RATIO)))[:-len(str(ALLY))] + BACK_RED + BLACK + (str(ENEMY) + str(' '*round(ENEMY*RATIO)))[:-len(str(ENEMY))] + NATIF

print(scoring)