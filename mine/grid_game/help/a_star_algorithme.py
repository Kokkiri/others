from time import sleep
import subprocess
from colorama import Back, Style
from random import random


GRID_SIZE = 30
SPEED = .1
RED = Back.RED
GREEN = Back.GREEN
YELLOW = Back.YELLOW
BLUE = Back.BLUE
BLACK = Back.BLACK
MAGENTA = Back.MAGENTA
CYAN = Back.CYAN

def clear():
    subprocess.run(['clear'])

class Cell():
    def __init__(self, pos):
        self.pos = pos
        self.neighbor = []
        self.f = 0
        self.g = 0
        self.h = 0
        self.previous = None
        self.type = None

    def get_pos(self):
        return self.pos
    
    def set_type(self, _type):
        self.type = _type
    
    def set_neighbors(self, neighbor):
        self.neighbor = neighbor

    def get_neighbors(self):
        return self.neighbor
    
    def is_wall(self):
        return self.type == 'wall'

class Grid():
    def __init__(self, size):

        self.grid = []
        for y in range(GRID_SIZE):
            row = []
            for x in range(GRID_SIZE):
                cell = Cell((x, y))
                if random() < 0.3:
                    cell.set_type('wall')
                row.append(cell)
            self.grid.append(row)

        self.start = self.grid[0][0]
        self.end = self.grid[GRID_SIZE-1][GRID_SIZE-1]
        self.end.set_type('end')
        self.open_set = [self.start]
        self.closed_set = []
        self.path = []
        self.current = self.start

    def get_all_neighbors(self):
        for y in range(GRID_SIZE):
            for x in range(GRID_SIZE):
                neighbor = []

                if x < GRID_SIZE-1 and not self.grid[y][x + 1].is_wall(): # Right
                    neighbor.append(self.grid[y][x + 1])

                if x > 0  and not self.grid[y][x - 1].is_wall(): # Left
                    neighbor.append(self.grid[y][x - 1])

                if y < GRID_SIZE-1 and not self.grid[y + 1][x].is_wall(): # Down
                    neighbor.append(self.grid[y + 1][x])

                if y > 0 and not self.grid[y - 1][x].is_wall(): # Up
                    neighbor.append(self.grid[y - 1][x])

                self.grid[y][x].set_neighbors(neighbor)

    def h(self, P1, P2):
        x1, y1 = P1
        x2, y2 = P2
        return abs(x1 - x2) + abs(y1- y2)

    def search_path(self):
        """A* Pathfinder Algorithm"""

        if self.open_set != []:
            win = 0
            for i in range(len(self.open_set)):
                if self.open_set[i].f < self.open_set[win].f:
                    win = i

            self.current = self.open_set[win]
            self.open_set.remove(self.current)
            self.closed_set.append(self.current)

            neighbors = self.current.get_neighbors()
            for neighbor in neighbors:
                if neighbor not in self.closed_set and not neighbor.is_wall():
                    temp_g = self.current.g + 1

                    newpath = False
                    if neighbor in self.open_set:
                        if temp_g < neighbor.g:
                            neighbor.g = temp_g
                            newpath = True
                    else:
                        neighbor.g = temp_g
                        self.open_set.append(neighbor)
                        newpath = True

                    if newpath:
                        neighbor.h = self.h(neighbor.get_pos(), self.end.get_pos())
                        neighbor.f = neighbor.g + neighbor.h
                        neighbor.previous = self.current

            self.path = []
            temp = self.current;
            self.path.append(temp);
            while temp.previous:
                self.path.append(temp.previous);
                temp = temp.previous
            
            if self.current == self.end:
                return 'reach'

        else:
            return 'no solution'

    def show(self):
        clear()
        color = ''
        for row in self.grid:
            line_to_print = ''
            for cell in row:

                # neighbors
                if cell in self.open_set:
                    color = YELLOW

                # start
                elif cell == self.start:
                    color = GREEN

                # current cell
                elif cell == self.current:
                    color = CYAN

                # current path
                elif cell in self.path:
                    color = MAGENTA

                # end
                elif cell == self.end:
                    color = BLUE

                # closed_set
                elif cell in self.closed_set:
                    color = RED

                # walls
                elif cell.is_wall():
                    color = BLACK

                # empty cell
                else:
                    color = ''

                line_to_print += color + ' '*2 + Style.RESET_ALL
            print(line_to_print)
        sleep(SPEED)

    def run(self):
        self.get_all_neighbors()
        while True:
            res = self.search_path()
            if res == 'reach':
                self.show()
                break
            if res == 'no solution':
                self.show()
                break
            self.show()

def main():
    try:
        Grid(GRID_SIZE).run()

    except KeyboardInterrupt:
        quit()

main()
