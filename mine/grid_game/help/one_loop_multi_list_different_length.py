from time import sleep
import subprocess
from colorama import Back, Style

def clear():
    subprocess.run(['clear'])

list_and_pos = [[[i for x in range(i+10)],0] for i in range(20)]

def show():

    while True:
        clear()
        for index in range(len(list_and_pos)):
            current_list = list_and_pos[index][0]
            current_pos = list_and_pos[index][1]

            if current_pos == 0:
                current_list[-1] = str(len(current_list)-1) + Style.RESET_ALL
                current_list[current_pos] = Back.RED + str(current_pos) + Style.RESET_ALL
            else:
                current_list[current_pos-1] = str(current_pos-1) + Style.RESET_ALL
                current_list[current_pos] = Back.RED + str(current_pos) + Style.RESET_ALL

            list_and_pos[index][1] += 1
            if list_and_pos[index][1] > len(current_list)-1:
                list_and_pos[index][1] = 0
            print(*current_list)
        sleep(.1)

show()
