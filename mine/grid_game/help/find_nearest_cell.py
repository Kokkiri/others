from time import sleep
from random import random
import subprocess
from colorama import Back, Style

SPEED = .1
GRID = []
CHAR = '_'
ONE = Back.MAGENTA + ' ' + Style.RESET_ALL
TWO = Back.RED + ' ' + Style.RESET_ALL
WALL_NUM = 2
GRID_SIZE = 30

for i in range(GRID_SIZE):
    row = []
    for i in range(GRID_SIZE):
        row.append(CHAR)
    GRID.append(row)

numbers = [2] * WALL_NUM + [1]
for n in numbers:
    y = int(random() * len(GRID))
    x = int(random() * len(GRID[0]))
    GRID[y].insert(x, n)
    GRID[y].pop()

POSITION_ONE = [(x, y) for y in range(len(GRID)) for x in range(len(GRID[0])) if GRID[y][x] == 1]
POSITION_TWO = [(x, y) for y in range(len(GRID)) for x in range(len(GRID[0])) if GRID[y][x] == 2]

def clear():
    subprocess.run(['clear'])

def show_grid():
    clear()
    for row in GRID:
        string = ''
        for i in row:
            if i == 2:
                i = TWO
            if i == 1:
                i = ONE
            string += i*2
        print(string)
    sleep(SPEED)

def search_nearest_index():
    distance = []
    for two in POSITION_TWO:
        one = (POSITION_ONE[0][0], POSITION_ONE[0][1])
        distance_one_two = lambda a, b: tuple(abs(i - j) for i, j in zip(a, b))
        distance.append(distance_one_two(one, two))
    
    dist_pos = []
    for i in zip(distance, POSITION_TWO):
        dist_pos.append([sum(i[0]), i[1]])
    return min(dist_pos)[1]

def check_adjacent_tiles(x, y):
    try:
        if GRID[y + 1][x] == 2 or GRID[y - 1][x] == 2 or GRID[y][x + 1] == 2 or GRID[y][x - 1] == 2:
            return 1
    except IndexError:
        pass

def loop():
    nearest_index = search_nearest_index()
    x_one, y_one = POSITION_ONE[0]
    x_two, y_two = nearest_index
    for y in range(len(GRID)):
        for x in range(len(GRID[0])):
            while check_adjacent_tiles(x_one, y_one) != 1:
                if x_one > x_two:
                    GRID[y_one].pop(x_one)
                    x_one -= 1
                    GRID[y_one].insert(x_one, 1)
                    show_grid()
                elif x_one < x_two:
                    GRID[y_one].pop(x_one)
                    x_one += 1
                    GRID[y_one].insert(x_one, 1)
                    show_grid()
                elif y_one > y_two:
                    GRID[y_one].pop(x_one)
                    GRID[y_one].insert(x_one, CHAR)
                    y_one -= 1
                    GRID[y_one].pop(x_one)
                    GRID[y_one].insert(x_one, 1)
                    show_grid()
                elif y_one < y_two:
                    GRID[y_one].pop(x_one)
                    GRID[y_one].insert(x_one, CHAR)
                    y_one += 1
                    GRID[y_one].pop(x_one)
                    GRID[y_one].insert(x_one, 1)
                    show_grid()
                else:
                    break

loop()
