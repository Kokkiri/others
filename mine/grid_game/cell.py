from threading import Thread
from playsound import playsound
import json
from random import random

with open("sounds.json", "r") as f:
    sound = json.load(f)


def death_sound():
    Thread(target=playsound, args=("sound/" + sound["death"],)).start()


def damage_sound():
    Thread(target=playsound, args=("sound/" + sound["damage"],)).start()


class Cell:
    """initialize cells"""

    def __init__(self, _type=None, value=0):

        self.value = value
        self.neighbors = []
        self.type = _type
        self.pos = None
        self.previous = None
        self.f = 0
        self.g = 0
        self.h = 0

    def reset(self):
        self.__init__(_type=None, value=0)

    def get_type(self):
        return self.type

    def set_type(self, _type):
        self.type = _type

    def set_value(self, value):
        self.value = int(self.value) + value

    def get_value(self):
        return self.value

    def set_pos(self, pos):
        self.pos = pos

    def get_pos(self):
        return self.pos

    def set_neighbors(self, neighbors):
        self.neighbors = neighbors

    def get_neighbors(self):
        return self.neighbors

    def init_previous(self):
        self.previous = None

    def is_wall(self):
        return self.type == "wall"

    def is_ally(self):
        return self.type == "ally"

    def is_enemy(self):
        return self.type == "enemy"

    def attack(self, defenseur, value):
        defenseur.set_value(value)
        if defenseur.get_value() <= 0:
            th_death_sound = Thread(target=death_sound)
            th_death_sound.start()
        else:
            th_damage_sound = Thread(target=damage_sound)
            th_damage_sound.start()
        return defenseur.get_value()
