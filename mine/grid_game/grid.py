from cell import Cell
from subprocess import run, call
from random import random
from time import sleep
from queue import LifoQueue
from playsound import playsound
from threading import Thread
import json
from constants import SPEED, EXPLOSION_RANGE, RATIO_BOMB, VOLUME


# FRONT COLOR
NATIF = "\033[0m"
BLACK = "\033[30m"
CYAN = "\033[96m"
YELLOW = "\033[93m"
PURPLE = "\033[95m"
BLUE = "\033[94m"
GREEN = "\033[92m"
RED = "\033[31m"
RED_LIGHT = "\033[91m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"

# BACK COLOR
BACK_GREEN_UP = "\033[48;5;46m"
BACK_YELLOW_DOWN = "\033[48;5;220m"
BACK_MAGENTA = "\033[48;5;198m"
BACK_PURPLE = "\033[48;5;54m"
BACK_PURPLE_DOWN = "\033[48;5;53m"
BACK_BLUE = "\033[48;5;27m"
BACK_BLUE_GREY = "\033[48;5;104m"
BACK_GREY = "\033[48;5;240m"
BACK_RED = "\033[48;5;196m"
BACK_BLACK = "\033[48;5;232m"

call(["amixer", "-D", "pulse", "sset", "Master", str(VOLUME) + "%"])

with open("sounds.json", "r") as f:
    sound = json.load(f)


def boom_sound():
    Thread(target=playsound, args=("sound/" + sound["boom"],)).start()


def big_boom_sound():
    Thread(target=playsound, args=("sound/" + sound["big_boom"],)).start()


class Grid:
    """initialize grid size and all cells"""

    def __init__(self, grid_size, ally, enemy):

        self.grid_size = grid_size
        self.grid = []

        for y in range(grid_size):
            row = []
            for x in range(grid_size):
                cell = Cell()
                row.append(cell)
            self.grid.append(row)

        self.ally = ally
        self.enemy = enemy

    def refresh(self):
        """refresh screen"""

        run(["clear"])
        self.show()
        sleep(SPEED)

    def set_positions(self, pieces):
        """aleatory position of cells on grid"""

        for piece in pieces:
            pos_x, pos_y = int(random() * self.grid_size), int(
                random() * self.grid_size
            )
            if self.grid[pos_y][pos_x].get_type() == None:
                self.grid[pos_y][pos_x] = piece
            else:
                pieces.append(piece)

    def get_all_neighbors(self):
        """let us know the neighbors of each cells"""

        for y in range(self.grid_size):
            for x in range(self.grid_size):
                self.grid[y][x].set_pos((x, y))
                self.grid[y][x].init_previous()
                neighbors = []

                if x < self.grid_size - 1:
                    neighbors.append(self.grid[y][x + 1])  # Right

                if x > 0:
                    neighbors.append(self.grid[y][x - 1])  # Left

                if y < self.grid_size - 1:
                    neighbors.append(self.grid[y + 1][x])  # Down

                if y > 0:
                    neighbors.append(self.grid[y - 1][x])  # Up

                self.grid[y][x].set_neighbors(neighbors)

    def search_path(self, cell, target, shortest_path_length):
        """A* Pathfinder Algorithm"""

        def heuristic(P1, P2):
            x1, y1 = P1
            x2, y2 = P2
            return abs(x1 - x2) + abs(y1 - y2)

        open_set = [cell]
        closed_set = []
        path = []
        current = cell

        while open_set != []:
            win = 0
            for i in range(len(open_set)):
                if open_set[i].f < open_set[win].f:
                    win = i

            current = open_set[win]
            open_set.remove(current)
            closed_set.append(current)

            neighbors = current.get_neighbors()
            for neighbor in neighbors:
                if (
                    neighbor not in closed_set
                    and (neighbor.get_type() == None or neighbor.get_type() == "mine")
                    or neighbor == target
                ):
                    temp_g = current.g + 1

                    newpath = False
                    if neighbor in open_set:
                        if temp_g < neighbor.g:
                            neighbor.g = temp_g
                            newpath = True
                    else:
                        neighbor.g = temp_g
                        open_set.append(neighbor)
                        newpath = True

                    if newpath:
                        neighbor.h = heuristic(neighbor.get_pos(), target.get_pos())
                        neighbor.f = neighbor.g + neighbor.h
                        neighbor.previous = current

            path = LifoQueue()
            temp = current
            while temp.previous:
                if len(path.queue) >= shortest_path_length:
                    return "no solution"
                path.put(temp.previous)
                temp = temp.previous

            if current == target:
                shortest_path_length = len(path.queue)
                return path, target, shortest_path_length

        else:
            return "no solution"

    def move_piece(self, cell, group):
        """choose the shortest path not the nearest cell"""

        if random() < RATIO_BOMB:
            deads = self.bomb_explode()
            return "bomb", deads

        shortest_path = None
        shortest_path_length = 100
        for c in group:
            _path = self.search_path(cell, c, shortest_path_length)
            if _path != "no solution":
                shortest_path, target, shortest_path_length = _path

        if shortest_path != None:
            shortest_path.get()
            current_cell = cell
            while not shortest_path.empty():
                x, y = current_cell.get_pos()
                next_x, next_y = shortest_path.get().get_pos()
                if self.grid[next_y][next_x].get_type() == "mine":
                    self.mine_explode(x, y, next_x, next_y, current_cell)
                    return "mine", current_cell
                else:
                    self.grid[next_y][next_x] = current_cell
                    self.grid[y][x] = Cell()
                    current_cell = self.grid[next_y][next_x]
                    self.refresh()
                    self.get_all_neighbors()
            return "reach", target

    def mine_explode(self, x, y, next_x, next_y, current_cell):
        """when current cell == mine, mine explode, cell disappears"""

        boom_sound()

        # la cellule apparaît à l'endroit de la mine
        self.grid[next_y][next_x] = current_cell
        self.grid[y][x] = Cell()
        self.refresh()

        # l'explosion débute à l'endroit de la mine
        self.grid[next_y][next_x] = Cell(_type="fire")
        self.refresh()

        # le feu s'éteind à l'endroit de la mine
        self.grid[next_y][next_x] = Cell()
        self.refresh()

    def bomb_explode(self):

        x = int(random() * self.grid_size)
        y = int(random() * self.grid_size)

        SOUTH = lambda i: (y + i, x)
        NORTH = lambda i: (y - i, x)
        RIGHT = lambda i: (y, x + i)
        LEFT = lambda i: (y, x - i)

        directions = {"SOUTH": SOUTH, "NORTH": NORTH, "RIGHT": RIGHT, "LEFT": LEFT}

        playsound("sound/falling_oganesson_freesound.mp3")

        big_boom_sound()
        # boom_sound()

        deads = []
        # l'explosion débute
        if self.grid[y][x].is_ally() or self.grid[y][x].is_enemy():
            deads.append(self.grid[y][x])
        self.grid[y][x] = Cell(_type="fire")
        self.refresh()

        wall = [False, False, False, False]
        # l'explosion se propage
        for i in range(1, EXPLOSION_RANGE):
            # boom_sound()
            n = 0
            for d in directions:
                if wall[n]:
                    pass
                else:
                    try:
                        if (
                            self.grid[directions[d](i)[0]][
                                directions[d](i)[1]
                            ].is_wall()
                            or directions[d](i)[1] < 0
                            or directions[d](i)[0] < 0
                        ):
                            wall[n] = True
                        elif (
                            self.grid[directions[d](i)[0]][
                                directions[d](i)[1]
                            ].is_ally()
                            or self.grid[directions[d](i)[0]][
                                directions[d](i)[1]
                            ].is_enemy()
                        ):
                            deads.append(
                                self.grid[directions[d](i)[0]][directions[d](i)[1]]
                            )
                            self.grid[directions[d](i)[0]][directions[d](i)[1]] = Cell(
                                _type="fire"
                            )
                        else:
                            self.grid[directions[d](i)[0]][directions[d](i)[1]] = Cell(
                                _type="fire"
                            )
                    except IndexError:
                        pass
                n += 1
            self.refresh()

        # le feu s'éteind à l'endroit de la mine
        self.grid[y][x] = Cell()
        self.refresh()

        # le feu se dissipe sur les cases adjacentes
        wall = [False, False, False, False]
        for i in range(1, EXPLOSION_RANGE):
            n = 0
            for d in directions:
                if wall[n] == True:
                    pass
                else:
                    try:
                        if (
                            self.grid[directions[d](i)[0]][
                                directions[d](i)[1]
                            ].is_wall()
                            or directions[d](i)[1] < 0
                            or directions[d](i)[0] < 0
                        ):
                            wall[n] = True
                        else:
                            self.grid[directions[d](i)[0]][directions[d](i)[1]].reset()
                    except IndexError:
                        pass
                n += 1
            self.refresh()

        return deads

    def score(self, ally, enemy):
        len_ally = len(ally)
        len_enemy = len(enemy)
        total = len_ally + len_enemy
        ratio = self.grid_size / total * 2
        return (
            BACK_BLUE
            + BLACK
            + (str(len_ally) + str(" " * round(len_ally * ratio)))[
                : -len(str(len_ally))
            ]
            + BACK_RED
            + BLACK
            + (str(len_enemy) + str(" " * round(len_enemy * ratio)))[
                : -len(str(len_enemy))
            ]
            + NATIF
        )

    def show(self):
        """affichage de la grille"""

        def box(cell):
            value = cell.get_value()
            _type = cell.get_type()
            color = NATIF

            if _type == "enemy":
                color = BACK_RED

            elif _type == "ally":
                color = BACK_BLUE

            elif _type == "mine":
                color = BACK_PURPLE_DOWN

            elif _type == "bomb":
                color = BACK_BLUE_GREY

            elif _type == "fire":
                color = BACK_YELLOW_DOWN

            elif _type == None:
                color = BACK_PURPLE

            else:
                color = NATIF

            empty_space = " " * (2 - len(str(value)))
            if value == 0:
                value = "  "
            else:
                value = str(value) + empty_space
            cell = color + BLACK + value + NATIF
            return cell

        print()
        for row_grid in self.grid:
            line_to_print = ""
            for cell in row_grid:
                line_to_print += box(cell)
            print(line_to_print)
        print(self.score(self.ally, self.enemy))
        print()


# function to test grid running python3 grid.py
# to run the game, launch python3 main.py
def main():

    main_grid = Grid(30)
    ally = [Cell(_type="ally") for i in range(2)]
    enemy = [Cell(_type="enemy") for i in range(2)]
    walls = [Cell(_type="wall") for i in range(300)]
    list_of_pieces = ally + enemy + walls
    main_grid.set_positions(list_of_pieces)

    main_grid.refresh()

    main_grid.get_all_neighbors()
    main_grid.move_piece(ally[0], enemy)

    main_grid.get_all_neighbors()
    main_grid.move_piece(enemy[0], ally)

    main_grid.get_all_neighbors()
    main_grid.move_piece(ally[1], enemy)

    main_grid.get_all_neighbors()
    main_grid.move_piece(enemy[1], ally)


if __name__ == "__main__":
    main()
