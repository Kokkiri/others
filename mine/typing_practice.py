from time import sleep
from datetime import datetime
from playsound import playsound
from pyfiglet import Figlet
from threading import Event, Lock, Thread
import curses
from queue import Queue, Empty

####################################################################
# import termios, sys , tty

# def _getch():
#     fd = sys.stdin.fileno()
#     old_settings = termios.tcgetattr(fd)
#     try:
#         tty.setraw(fd)
#         ch = sys.stdin.read(1)     #This number represents the length
#     finally:
#         termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
#     return ch
####################################################################


INPUT_EXIT = 1

sequence = "Pour Sabina, vivre dans la vérité, ne mentir ni à soi-même ni aux autres, ce n'est possible qu'à la condition de vivre sans public. Dès lors qu'il y a un témoin à nos actes, nous nous adaptons bon gré mal gré aux yeux qui nous observent, et plus rien de ce que nous faisons n'est vrai. "
list_seq = list(sequence)
green = "\033[92m"
red = "\033[31m"
default_color = "\033[0;00m"


def setup():
    try:
        curses.curs_set(False)
    except curses.error:
        # fails on some terminals
        pass

    curses_lock = Lock()
    input_queue = Queue()
    quit_event = Event()

    return (curses_lock, input_queue, quit_event)


def input_thread_body(stdscr, curses_lock, input_queue, quit_event):
    while not quit_event.is_set():
        try:
            with curses_lock:
                key = stdscr.getkey()
        except:
            key = None
        if key in ("q", "Q"):
            input_queue.put(INPUT_EXIT)


def typing(stdscr):

    curses_lock, input_queue, quit_event = setup()

    input_thread = Thread(
        target=input_thread_body, args=(stdscr, curses_lock, input_queue, quit_event)
    )
    input_thread.start()

    i = 0

    try:
        while i < 30:

            try:
                input_action = input_queue.get(True, 1)
            except Empty:
                input_action = None
            if input_action == INPUT_EXIT:
                break

            # for i, char in enumerate(list_seq):
            #     if _getch() == char:
            #         list_seq[i] = green + char + default_color
            #     else:
            #         list_seq[i] = red + char + default_color
            #     print(''.join(list_seq))

            stdscr.clear()
            stdscr.addstr(0, 0, sequence)
            stdscr.refresh()

            # stdscr.addstr(0, 0, sequence)

            i += 1

    finally:
        quit_event.set()
        input_thread.join()


if __name__ == "__main__":
    curses.wrapper(typing)
