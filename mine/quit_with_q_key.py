from time import sleep
import curses
import os


def ma_boucle(stdscr):
    stdscr.addstr(0, 0, "press q to quit")

    while True:
        try:
            key = stdscr.getkey()
        except:
            key = None
        if key in ("q", "Q"):
            break


if __name__ == "__main__":
    curses.wrapper(ma_boucle)
