import os
import time

# from gen_laby import generate_labyrinthe
from handle_img_lab import gen_matrice

# matrice = [
#     [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
#     [1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
#     [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
#     [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
#     [1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1],
#     [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
#     [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1],
#     [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
#     [1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1],
#     [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
#     [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# ]

matrice = gen_matrice()

cell_start = (1, 1)
cell_end = (len(matrice) - 2, len(matrice[0]) - 2)
path = []
current_cell = cell_start


def NORTH(cell):
    y, x = cell
    return y - 1, x


def EAST(cell):
    y, x = cell
    return y, x + 1


def SOUTH(cell):
    y, x = cell
    return y + 1, x


def WEST(cell):
    y, x = cell
    return y, x - 1


directions = {"West": WEST, "East": EAST, "South": SOUTH, "North": NORTH}


def cell_value(cell):
    y, x = cell
    try:
        return matrice[y][x]
    except IndexError:
        return 1


def search_next_cell(cell):
    y, x = cell

    for direction in directions:
        if cell_value(directions[direction](cell)) == 0:
            matrice[y][x] = 2
            path.append(direction)
            return directions[direction](cell)

    for direction in directions:
        if cell_value(directions[direction](cell)) == 2:
            matrice[y][x] = 3
            path.pop()
            return directions[direction](cell)


while current_cell != cell_end:
    current_cell = search_next_cell(current_cell)
    time.sleep(0.06)
    os.system("clear")
    for n in matrice:
        res = ""
        for i in n:
            if i == 0:
                i = "\033[8;31m \033[0m"
            if i == 1:
                i = "\033[7;35m \033[0m"
            if i == 2:
                i = "\033[7;32m \033[0m"
            if i == 3:
                i = "\033[7;34m \033[0m"
            res += str(i) + str(i)
        print(res)
