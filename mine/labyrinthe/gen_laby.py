# 	https://github.com/juju21555/PythonMaze/tree/master/python/algorithmeGeneration

import random


def breakWall(x1, y1, x2, y2):
    milieu = [x1 + x2 + 1, y1 + y2 + 1]
    murs[milieu[1]][milieu[0]] = 0


def generate_labyrinthe(xMax, yMax):
    """Exploration exhaustive"""

    global murs

    directionX = {"N": 0, "S": 0, "E": 1, "O": -1}
    directionY = {"N": 1, "S": -1, "E": 0, "O": 0}

    labyrinthe = [[0 for x in range(xMax)] for y in range(yMax)]
    murs = [[1 for x in range(2 * xMax + 1)] for y in range(2 * yMax + 1)]
    for y in range(len(murs) - 1):
        for x in range(len(murs[y]) - 1):
            if x % 2 != 0 and y % 2 != 0:
                murs[y][x] = 0

    randCell = ((random.randrange(xMax)), (random.randrange(yMax)))
    lastCell = [randCell]

    while lastCell != []:
        (rx, ry) = lastCell[-1]
        labyrinthe[ry][rx] = 1
        lVoisins = []
        for direction in ["N", "S", "O", "E"]:
            nx = rx + directionX[direction]
            ny = ry + directionY[direction]
            if nx >= 0 and nx < xMax and ny >= 0 and ny < yMax:

                if labyrinthe[ny][nx] == 0:

                    lVoisins.append(direction)

        if len(lVoisins) > 0:
            var = random.choice(lVoisins)
            breakWall(rx, ry, (rx + directionX[var]), (ry + directionY[var]))
            rx, ry = rx + directionX[var], ry + directionY[var]
            lastCell.append((rx, ry))
        else:
            lastCell.pop()

    return murs


def generate_labyrinthe2(xMax, yMax):
    """Fusion aléatoire"""

    global murs

    directionX = {"N": 0, "S": 0, "E": 1, "O": -1}
    directionY = {"N": 1, "S": -1, "E": 0, "O": 0}

    labyrinthe = [[(x + xMax * y) for x in range(xMax)] for y in range(yMax)]

    murs = [[1 for x in range(2 * xMax + 1)] for y in range(2 * yMax + 1)]
    for y in range(len(murs) - 1):
        for x in range(len(murs[y]) - 1):
            if x % 2 != 0 and y % 2 != 0:
                murs[y][x] = 0

    openedWall = 0

    while openedWall < xMax * yMax - 1:
        (rx, ry) = ((random.randrange(xMax)), (random.randrange(yMax)))
        direction = random.choice(["N", "S", "O", "E"])
        nx = rx + directionX[direction]
        ny = ry + directionY[direction]
        if nx >= 0 and nx < xMax and ny >= 0 and ny < yMax:

            if labyrinthe[ny][nx] != labyrinthe[ry][rx]:

                breakWall(rx, ry, nx, ny)
                var = labyrinthe[ny][nx]
                for y in range(yMax):
                    for x in range(xMax):
                        if labyrinthe[y][x] == var:
                            labyrinthe[y][x] = labyrinthe[ry][rx]
                rx, ry = nx, ny
                openedWall += 1

    return murs


# import pprint
# pprint.pprint(generate_labyrinthe(5,5))
