from math import floor

times = [0.55, 0.56, 1.27, 3.11, 1.19, 1.1, 1.17, 1.7, 1.21, 1.26, 1.3, 1.9, 1.5, 1.5]


def convert_hour_to_minute(times):
    total_minute = 0
    for time in times:
        hour_to_minute = floor(time) * 60
        if isinstance(time, float):
            minute_rest = int(str(time).split(".")[1].ljust(2, "0"))
        else:
            minute_rest = int([str(time), "0"][1].ljust(2, "0"))
        time_to_minute = hour_to_minute + minute_rest
        total_minute += time_to_minute
    convert_total_minute_to_hour(total_minute)


def convert_total_minute_to_hour(total_minute):
    hour = int(total_minute / 60)
    minute = total_minute - hour * 60
    print(hour, minute)


convert_hour_to_minute(times)
