#!/usr/bin/env python3

from data.nombres import gen_nombres
from ped.pedantix import run

mots_file = open(
    "/home/edouard/Documents/python/scripts/mine/cemantix/data/mots.txt", "r"
)
mots = mots_file.read().split("\n")

articles_file = open(
    "/home/edouard/Documents/python/scripts/mine/cemantix/data/articles.txt", "r"
)
articles = articles_file.read().split("\n")

liste = articles + mots + gen_nombres(2030)

run(liste)
