#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from time import sleep

driver = webdriver.Chrome()
driver.get("https://cemantix.certitudes.org/")
driver.find_element(By.CLASS_NAME, "fc-close").click()
driver.find_element(By.ID, "dialog-close").click()
driver.find_element(By.ID, "pedantix").click()
driver.find_element(By.ID, "dialog-close").click()
elem = driver.find_element(By.ID, "pedantix-guess")


def run(dico):
    for el in dico:
        elem.clear()
        sleep(0.1)
        elem.send_keys(el)
        elem.send_keys(Keys.RETURN)


#         WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'pedantix-guess-btn')))
#     assert "No results found." not in driver.page_source
# driver.close()
