#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from time import sleep

driver = webdriver.Chrome()
driver.get("https://cemantix.certitudes.org/")
pub = driver.find_element(By.CLASS_NAME, "fc-close").click()
pub2 = driver.find_element(By.ID, "dialog-close").click()
elem = driver.find_element(By.ID, "cemantix-guess")

with open(
    "/home/edouard/Documents/python/scripts/mine/cemantix/data/mots.txt", "r"
) as file:
    for el in file:
        elem.send_keys(el)
        elem.send_keys(Keys.RETURN)
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "cemantix-guess-btn"))
        )
