# https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif

import glob
from PIL import Image

# from pathlib import Path
import sys

FOLDER = sys.argv[1]

# CURRENT_PATH = Path(__file__).parent
# PATH = CURRENT_PATH / FOLDER

# filepaths
fp_out = "./image.gif"

# images = sorted(glob.glob(f"{PATH}/*.png"))
images = sorted(glob.glob(FOLDER + "/*.png"))

img, *imgs = [Image.open(f).resize((600, 500)) for f in images]
img.save(
    fp=fp_out, format="GIF", append_images=imgs, save_all=True, duration=50, loop=0
)
