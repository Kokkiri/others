import os, time

os.system("clear")
filenames = ["toto.txt", "titi.txt", "tata.txt"]
frames = []

for name in filenames:
    with open(name, "r", encoding="utf8") as f:
        frames.append(f.readlines())

while True:
    for frame in frames:
        print("".join(frame))
        time.sleep(0.05)
        os.system("clear")
