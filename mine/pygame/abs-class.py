# show how to use abstact class
# how to pass arguments through fufctions and class
# how to use optionals arguments


class Base:
    def __init__(self):
        self.nombre = 5

    def main(self, bla="di", blo="arg2"):
        self.one(bla, blo)

    def one(self, bla, blo):
        print("non", bla, blo)


# name wished argument if you want to ignore the others
class Menu(Base):
    def __init__(self):
        # now Base is inherit by menu we can call main() with self.main()
        self.main(blo="diou")


# we can call a method after instanciate a class
p = Base()
print(p.nombre)

Menu()
