import pygame
import block as blo


class Wall(pygame.sprite.Group):

    def __init__(self, screen, column_nb, row_nb):

        super().__init__()

        count = 0
        background = pygame.Surface(screen.get_size())
        bg_width = background.get_width()
        bg_height = background.get_height()
        wall_width = 55 * column_nb
        wall_height = 35 * row_nb
        x_pos = (bg_width - wall_width) / 2
        y_pos = 20

        for row in range(row_nb):
            for column in range(column_nb):
                if count < row_nb:
                    b = blo.Block(55 * column + x_pos, 35 * count + y_pos, 50, 30)
                    self.add(b)

            count += 1
