import pygame
from random import randint


class Block(pygame.sprite.Sprite):

    def __init__(self, x, y, width, height):

        super().__init__()

        self.image = pygame.Surface((width, height))
        self.image.fill((200, 200, 0))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
