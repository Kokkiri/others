import pygame
import paddle as pad
import ball as bal
import wall as wal


# ==========================|  INIT  |===================================

pygame.init()
screen = pygame.display.set_mode([600, 500])
background = pygame.Surface(screen.get_size())
paddle = pad.Paddle(100, 30, screen.get_rect())
ball = bal.Ball(screen)
wall = wal.Wall(screen, 9, 5)
paddle_group = pygame.sprite.GroupSingle(paddle)
ball_group = pygame.sprite.Group(ball)
allSprite = pygame.sprite.Group(ball_group, paddle_group, wall)

# ==========================|  DRAW  |==================================


def drawScreen():
    allSprite.clear(screen, background)
    screen.fill((255, 255, 200))
    allSprite.update()
    allSprite.draw(screen)
    pygame.display.update()


# ==========================|  LOOP  |=================================


def text_objects(text, font):
    textSurface = font.render(text, True, (0, 0, 0))
    return textSurface, textSurface.get_rect()


def game_intro():
    loop = True
    while loop:
        clock = pygame.time.Clock()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_s:
                    return "level_one"
                if event.key == pygame.K_q:
                    return "quit"

        screen.fill((255, 255, 255))
        largeText = pygame.font.Font("freesansbold.ttf", 115)
        TextSurf, TextRect = text_objects("BreackouT", largeText)
        TextRect.center = ((600 / 2), (500 / 2))
        screen.blit(TextSurf, TextRect)
        pygame.display.update()
        clock.tick(15)


def first_level():
    loop = True
    while loop:
        clock = pygame.time.Clock()
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return "quit"
                if event.key == pygame.K_i:
                    return "intro"

        keys = pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:
            paddle.moveRight()
        elif keys[pygame.K_LEFT]:
            paddle.moveLeft()

        if pygame.sprite.spritecollide(paddle, ball_group, False):
            # ball.speedY *= -1
            if ball.rect.bottom > paddle.rect.top:
                ball.speedY *= -1
            # if ball.speedX > 0:
            #     ball.rect.right = paddle.rect.left
            # if ball.speedX < 0:
            #     ball.rect.left = paddle.rect.right
            # if ball.speedY > 0:
            #     ball.rect.bottom = paddle.rect.top
            # if ball.speedX < 0:
            #     ball.rect.top = paddle.rect.bottom

        block_list = pygame.sprite.spritecollide(ball, wall, True)
        for block in block_list:
            ball.speedY *= -1

        drawScreen()


# ==========================|  END  |===================================

action = {"intro": game_intro, "level_one": first_level}
state = "intro"

loop = True
while loop:
    state = action[state]()
    if state == "quit":
        loop = False

pygame.quit()
