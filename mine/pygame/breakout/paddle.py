import pygame


class Paddle(pygame.sprite.Sprite):
    """hold paddle"""

    def __init__(self, width, height, rect):

        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill((200, 100, 100))

        self.context = rect
        self.rect = self.image.get_rect()
        self.rect.center = (rect.centerx, rect.height * 0.8)

    def moveLeft(self):
        self.rect.x -= 10
        if self.rect.x < self.context.x:
            self.rect.x = self.context.x

    def moveRight(self):
        self.rect.x += 10
        if self.rect.x > self.context.width - self.rect.width:
            self.rect.x = self.context.width - self.rect.width
