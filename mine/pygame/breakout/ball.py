import pygame


class Ball(pygame.sprite.Sprite):
    """hold balle"""

    def __init__(self, screen):
        # Call the parent class (Sprite) constructor
        super().__init__()

        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.screen = screen
        self.speedX = 5
        self.speedY = 5

        self.image = pygame.Surface((30, 30), pygame.SRCALPHA, 32)
        self.image.convert_alpha()
        self.image.fill((0, 0, 0, 0))
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
        # Update the position of this object by setting the values of rect.x and rect.y
        pygame.draw.circle(self.image, (255, 100, 100), (15, 15), 15)

    def update(self):

        self.rect.x += self.speedX
        self.rect.y += self.speedY
        if self.rect.x < 0 or self.rect.x > self.screen.get_width() - 30:
            self.speedX *= -1
        if self.rect.y < 0 or self.rect.y > self.screen.get_height() - 30:
            self.speedY *= -1
