import pygame
import pymunk
import pymunk.pygame_util
import math

pygame.init()

WIDTH, HEIGHT = 1000, 700
window = pygame.display.set_mode((WIDTH, HEIGHT))


def draw(space, window, draw_options):
    window.fill("white")
    space.debug_draw(draw_options)
    pygame.display.update()


def create_boundaries(space, width, height):
    rects = [
        [(width / 2, height - 10), (width, 3)],
        [(width / 2, 10), (width, 3)],
        [(10, height / 2), (3, height)],
        [(width - 10, height / 2), (3, height)],
    ]

    for pos, size in rects:
        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        body.position = pos
        shape = pymunk.Poly.create_box(body, size)
        shape.elasticity = 0
        shape.friction = 0
        shape.color = (0, 0, 0, 0)
        space.add(body, shape)


def create_wheel(space, radius, mass):
    # CREATE BODY
    body = pymunk.Body()
    body.position = (300, 300)
    # AFFECT SHAPE TO BODY
    shape = pymunk.Circle(body, radius)
    shape.mass = mass
    shape.elasticity = 0
    shape.friction = 0
    shape.color = (255, 0, 0, 50)
    space.add(body, shape)
    return shape


def run(window, width, height):
    run = True
    clock = pygame.time.Clock()
    fps = 60
    # DELTA TIME OR DIFFERENCE IN TIME
    dt = 1 / fps

    space = pymunk.Space()
    space.gravity = (0, 981)

    wheel = create_wheel(space, 30, 10)
    # CERATE WALLS
    create_boundaries(space, width, height)

    draw_options = pymunk.pygame_util.DrawOptions(window)

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

        draw(space, window, draw_options)
        space.step(dt)
        clock.tick(fps)

    pygame.quit()


if __name__ == "__main__":
    run(window, WIDTH, HEIGHT)
