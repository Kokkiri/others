import pygame


class Screen(object):
    def __init__(self):
        pygame.init()
        size = (640, 480)
        self.screen = pygame.display.set_mode(size)

    def toggle(self):
        if self.screen.get_flags() == 0:
            self.screen = pygame.display.set_mode((640, 480), pygame.FULLSCREEN)
        else:
            self.screen = pygame.display.set_mode((640, 480))
