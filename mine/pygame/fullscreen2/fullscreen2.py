import pygame
from screen import Screen
from square import Square

s = Screen()
b = Square()


def main_loop():
    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    run = False
                if event.key == pygame.K_f:
                    s.toggle()

        b.draw(s.screen)
        pygame.display.flip()


if __name__ == "__main__":
    main_loop()
