import os
import pygame
from pygame.locals import *

width = 500
height = 500
pygame.init()
screen = pygame.display.set_mode((width, height))
img = pygame.image.load("../img/fio_stand_0.png")
screen.blit(pygame.transform.scale(img, (width, height)), (0, 0))
pygame.display.flip()

run = True
while run:
    # Je sais pas à quoi ça sert:
    # pygame.event.pump()
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
            if event.key == pygame.K_f:
                if pygame.Surface.get_flags(screen) == 0:
                    pygame.display.set_mode((width, height), pygame.FULLSCREEN)
                else:
                    pygame.display.set_mode((width, height))

                screen.blit(pygame.transform.scale(img, (width, height)), (0, 0))
                pygame.display.flip()
