import pygame as pg
from random import randint


pg.init()
screen = pg.display.set_mode([350, 350])

count = 0
list = []


run = True
while run:

    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_q:
                run = False

    for row in range(6):
        for column in range(6):
            if count < 6:
                fr = pg.draw.rect(
                    screen,
                    (randint(0, 255), randint(0, 255), randint(0, 255)),
                    (column * 55, count * 55, 50, 50),
                )
        count += 1

    pg.display.flip()
