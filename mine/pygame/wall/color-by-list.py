import pygame, random


pygame.init()

width = 400
height = 400
size = (width, height)
screen = pygame.display.set_mode(size)

# VARIABLES --------------------------------
clock = pygame.time.Clock()

BLACK = (0, 0, 0)

num = 15
x = num
y = num
color = [0] * num * num

for i in range(num * num):
    color[i] = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))


# MAIN LOOP --------------------------------
run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False

    # INPUT --------------------------------

    # PROCESS --------------------------------

    # DRAW --------------------------------
    screen.fill(BLACK)

    for i in range(1, num):
        for j in range(1, num):
            pygame.draw.rect(screen, color[j * i], (x * i, y * j, num, num))

    pygame.display.flip()
    clock.tick(30)
