import pygame, random


pygame.init()


# SURFACE   -------------------------------
num = 15
width = (num + 5) * num + num
height = (num + 5) * num + num
size = (width, height)
screen = pygame.display.set_mode(size)

# VARIABLES -------------------------------
clock = pygame.time.Clock()

color = [0] * (num * num)
white = (255, 255, 255)
x = num + 5
y = num + 5

for i in range(len(color)):
    color[i] = (
        random.randrange(0, 255),
        random.randrange(0, 255),
        random.randrange(0, 255),
    )
# MAIN LOOP -------------------------------
run = True
while run:

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False

    # DRAW  -------------------------------
    screen.fill(white)

    for i in range(1, num):
        for j in range(1, num):
            pygame.draw.rect(screen, color[i * j], (x * i, y * j, num, num))

    pygame.display.flip()
    clock.tick(60)
