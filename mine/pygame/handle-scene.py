import pygame, random
import sys

# import pygame.freetype

# ==========================|  INIT  |===================================

pygame.init()
screen = pygame.display.set_mode([400, 400])
background = pygame.Surface(screen.get_size())
stand = [
    pygame.image.load("./img/one.png"),
    pygame.image.load("./img/two.png"),
    pygame.image.load("./img/three.png"),
    pygame.image.load("./img/four.png"),
    pygame.image.load("./img/three.png"),
    pygame.image.load("./img/two.png"),
]
clock = pygame.time.Clock()
standCount = 0

# ==========================|  DRAW  |===================================


def text_objects(text, font):
    textSurface = font.render(text, True, (0, 0, 0), (255, 255, 255))
    return textSurface, textSurface.get_rect()


def draw(text, color, angle):
    screen.fill(color)
    pygame.transform.scale(stand[standCount // 6], (200, 200))
    images = pygame.transform.rotate(stand[standCount // 6], angle)
    images_rect = images.get_rect()
    screen.blit(images, images_rect)
    largeText = pygame.font.Font("freesansbold.ttf", 70)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((400 / 2), (400 / 2))
    screen.blit(TextSurf, TextRect)
    pygame.display.update()


# ==========================|  LOOP  |=================================


def one():
    global standCount
    text = "ONE"
    color = (255, 100, 0)
    angle = 0
    loop = True
    open_door_one = False
    open_door_two = False

    while loop:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return "quit"
                if event.key == pygame.K_LEFT:
                    open_door_one = True
                if event.key == pygame.K_RIGHT:
                    open_door_two = True

        if standCount + 1 >= 36:
            standCount = 0

        standCount += 1

        if open_door_one == True and standCount == 4:
            return "four"
        if open_door_two == True and standCount == 20:
            return "two"

        draw(text, color, angle)


def two():
    global standCount
    text = "TWO"
    color = (0, 255, 100)
    angle = 90
    loop = True
    open_door_one = False
    open_door_two = False

    while loop:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return "quit"
                if event.key == pygame.K_LEFT:
                    open_door_one = True
                if event.key == pygame.K_RIGHT:
                    open_door_two = True

        if standCount + 1 >= 36:
            standCount = 0

        standCount += 1

        if open_door_one == True and standCount == 4:
            return "one"
        if open_door_two == True and standCount == 20:
            return "three"

        draw(text, color, angle)


def three():
    global standCount
    text = "THREE"
    color = (255, 0, 100)
    angle = 180
    loop = True
    open_door_one = False
    open_door_two = False

    while loop:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return "quit"
                if event.key == pygame.K_LEFT:
                    open_door_one = True
                if event.key == pygame.K_RIGHT:
                    open_door_two = True

        if standCount + 1 >= 36:
            standCount = 0

        standCount += 1

        if open_door_one == True and standCount == 20:
            return "four"
        if open_door_two == True and standCount == 4:
            return "two"

        draw(text, color, angle)


def four():
    global standCount
    text = "FOUR"
    color = (100, 255, 0)
    angle = 270
    loop = True
    open_door_one = False
    open_door_two = False

    while loop:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    return "quit"
                if event.key == pygame.K_LEFT:
                    open_door_one = True
                if event.key == pygame.K_RIGHT:
                    open_door_two = True

        if standCount + 1 >= 36:
            standCount = 0

        standCount += 1

        if open_door_one == True and standCount == 20:
            return "one"
        if open_door_two == True and standCount == 4:
            return "three"

        draw(text, color, angle)


# ==========================|  END  |===================================


def main_loop():
    action = {
        "one": one,
        "two": two,
        "three": three,
        "four": four,
    }
    state = "one"

    while True:
        state = action[state]()
        if state == "quit":
            break

    pygame.quit()


if __name__ == "__main__":
    main_loop()
