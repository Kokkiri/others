import pygame
from pathlib import Path
import glob

pygame.init()
width, height = 800, 800
screen = pygame.display.set_mode((width, height))

###################################################

CURRENT_PATH = Path(__file__).parent
PATH = CURRENT_PATH / "text"

text_files = sorted(glob.glob(str(PATH / "*.txt")))

texts = [open(one_file, "r").readlines() for one_file in text_files]

###################################################


def text_objects(text, font, color_text=(255, 255, 255), color_bg=(0, 0, 0)):
    textSurface = font.render(text, True, color_text, color_bg)
    return textSurface, textSurface.get_rect()


def display_text(text, pos_x, pos_y, size=40):
    largeText = pygame.font.SysFont("m3x6", size)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = (pos_x, pos_y)
    screen.blit(TextSurf, TextRect)


def display_frame(text):
    dec_y = 0
    for line in text:
        display_text(line, width / 2, dec_y)
        dec_y += 25


# def display_frame(text):
# 	dec_x = 0
# 	dec_y = 0
# 	for line in text:
# 		for letter in line:
# 			display_text(letter, dec_x - 500, dec_y + 500)
# 			dec_x += 20
# 		dec_y += 25

###################################################


def drawScreen(text):
    screen.fill((0, 0, 0))
    display_frame(text)
    pygame.display.update()


clock = pygame.time.Clock()
count = 0
tick = 20

run = True
while run:

    clock.tick(tick)

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
            if event.key == pygame.K_UP:
                tick += 2
            if event.key == pygame.K_DOWN:
                tick -= 2

    if count + 1 >= len(text_files):
        count = -1

    text = texts[count]

    count += 1

    if tick < 0:
        tick = 0

    drawScreen(text)

pygame.quit()
