import pygame
import json
import glob
from pathlib import Path

pygame.init()
width, height = 1000, 800
screen = pygame.display.set_mode((width, height))

CURRENT_PATH = Path(__file__).parent
PATH = CURRENT_PATH / "img"

anim = []
path = sorted(glob.glob(str(PATH / "*.jpg")))
for p in path:
    anim.append(pygame.image.load(p))

clock = pygame.time.Clock()
count = 0
tick = 15


def drawScreen():
    screen.fill((100, 150, 200))
    images = pygame.transform.scale(anim[count], (width, height))
    images_rect = images.get_rect()
    screen.blit(images, images_rect)
    pygame.display.update()


run = True
while run:

    clock.tick(tick)

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
            if event.key == pygame.K_UP:
                tick += 2
            if event.key == pygame.K_DOWN:
                tick -= 2

    if count + 1 >= len(anim):
        count = -1

    count += 1

    if tick < 0:
        tick = 0

    drawScreen()

pygame.quit()
