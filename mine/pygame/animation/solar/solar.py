import pygame
from utils import _next, cosin, countangle
from math import sin, cos


class Solar:
    def __init__(self, screen):
        self.screen = screen

    def loop(self):
        pygame.init()
        clock = pygame.time.Clock()
        tick = 28
        dt = 1 / 60
        dt2 = 1 / 20
        earth_angle = 0
        moon_angle = 0
        scene = "1"
        run = True
        system_angle = 100
        system_angle_count = 1 / 60
        while run:
            clock.tick(tick)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        return "quit"
                    if event.key == pygame.K_LEFT:
                        return _next(scene, 3, -1)
                    if event.key == pygame.K_RIGHT:
                        return _next(scene, 3, 1)
            keys = pygame.key.get_pressed()
            if keys[pygame.K_UP]:
                if system_angle >= 200 or system_angle <= 0:
                    system_angle_count *= -1
                system_angle -= system_angle_count
            if keys[pygame.K_DOWN]:
                if system_angle >= 200 or system_angle <= 0:
                    system_angle_count *= -1
                system_angle += system_angle_count

            earth_angle += dt
            moon_angle += dt2

            self.draw(earth_angle, moon_angle, system_angle)

    def draw(self, earth_angle, moon_angle, system_angle):
        ################    INIT    ###################
        dist_earth_sun = 200
        dist_moon_earth = 60
        screen_x_center = self.screen.get_width() / 2
        screen_y_center = self.screen.get_height() / 2
        rotate_y_earth = cosin(0, dist_earth_sun, sin, system_angle)
        rotate_y_moon = cosin(0, dist_moon_earth, sin, system_angle)
        rotate_y_angle_text = cosin(0, 100, sin, system_angle)
        rotate_y_trajectory = cosin(0, 300, sin, system_angle)
        rotate_y_trajectory_2 = cosin(0, 310, sin, system_angle)
        ###############################################

        ###############    PLANETS    #################
        # earth
        x = 0
        y = 0
        (x, y) = cosin(screen_x_center, dist_earth_sun, cos, earth_angle), cosin(
            screen_y_center, rotate_y_earth, sin, earth_angle
        )

        # moon
        x2 = 0
        y2 = 0
        (x2, y2) = cosin(x, -dist_moon_earth, cos, moon_angle), cosin(
            y, rotate_y_moon, sin, moon_angle
        )
        ###############################################

        ##############    SHOW ANGLE    ###############
        # angle text position around earth
        x_agl = 0
        y_agl = 0
        (x_agl, y_agl) = cosin(x, 100, cos, earth_angle), cosin(
            y, rotate_y_angle_text, sin, earth_angle
        )

        # angle text render
        angle = pygame.font.Font("fonts/munro.ttf", 40).render(
            countangle(earth_angle), False, (150, 150, 150), (0, 0, 0)
        )
        angle_rect = angle.get_rect()
        angle_rect.center = (x_agl, y_agl)
        ###############################################

        ########    DRAW LINE AROUND SYSTEM    ########
        # line x y
        x_tra = [cosin(screen_x_center, 300, cos, n, 180) for n in range(0, 360, 5)]
        y_tra = [
            cosin(screen_y_center, rotate_y_trajectory, sin, n, 180)
            for n in range(0, 360, 5)
        ]
        trajectory_coordinate = [(x_tra[i], y_tra[i]) for i in range(len(x_tra))]

        # line x2 y2
        x_tra = [cosin(screen_x_center, 310, cos, n, 180) for n in range(0, 360, 5)]
        y_tra = [
            cosin(screen_y_center, rotate_y_trajectory_2, sin, n, 180)
            for n in range(0, 360, 5)
        ]
        trajectory_coordinate_2 = [(x_tra[i], y_tra[i]) for i in range(len(x_tra))]

        # line coordinate
        line_coordinate = [
            (trajectory_coordinate[i], trajectory_coordinate_2[i])
            for i in range(len(trajectory_coordinate))
        ]
        ###############################################

        #############    DRAW ELEMENTS    #############
        self.screen.fill((0, 0, 0))
        sun = pygame.draw.circle(
            self.screen,
            (200, 200, 0),
            (round(screen_x_center), round(screen_y_center)),
            50,
            1,
        )
        earth = pygame.draw.circle(self.screen, (0, 100, 200), (x, y), 30, 1)
        moon = pygame.draw.circle(self.screen, (150, 150, 150), (x2, y2), 10, 1)
        for n in line_coordinate:
            pygame.draw.line(self.screen, (150, 150, 150), n[0], n[1])
        self.screen.blit(angle, angle_rect)
        pygame.display.update()
        ###############################################
