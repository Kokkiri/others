import pygame
from utils import _next, selector
from pathlib import Path
from glob import glob
from env import WIDTH, HEIGHT

CURRENT_PATH = Path(__file__).parent
PATH = CURRENT_PATH / "img"

anim = []
path = sorted(glob(str(PATH / "*.png")))
for p in path:
    anim.append(pygame.image.load(p))


class Patio:
    def __init__(self, screen):
        self.screen = screen

    def loop(self):
        pygame.init()
        clock = pygame.time.Clock()
        tick = 28
        dt = 1 / tick
        scene = "3"
        option = "2"
        anim_count = 0
        count = 0
        lastTime = 0
        interval = 0.15
        run = True
        while run:
            clock.tick(tick)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        return "quit"
                    if event.key == pygame.K_LEFT and option == "2":
                        return _next(scene, 3, -1)
                    if event.key == pygame.K_RIGHT and option == "2":
                        return _next(scene, 3, 1)
            keys = pygame.key.get_pressed()
            # chose factor
            if keys[pygame.K_UP] and (count > lastTime + interval):
                lastTime = count
                option = _next(option, 2, 1)
            if keys[pygame.K_DOWN] and (count > lastTime + interval):
                lastTime = count
                option = _next(option, 2, -1)

            # chose frame
            if keys[pygame.K_LEFT] and option == "1":
                if anim_count == 0:
                    anim_count = 0
                else:
                    anim_count -= 1
            if keys[pygame.K_RIGHT] and option == "1":
                if anim_count == len(anim) - 1:
                    anim_count = len(anim) - 1
                else:
                    anim_count += 1

            count += dt

            self.draw(count, option, anim_count)

    def draw(self, count, option, anim_count):
        self.screen.fill((100, 150, 200))
        images = pygame.transform.scale(anim[anim_count], (WIDTH, HEIGHT))
        images_rect = images.get_rect()
        self.screen.blit(images, images_rect)
        selector(self.screen, "2", option, "< CHANGE SCENE >", (10, 10))
        color = (100, 100, 100)
        if option == "1":
            color = (255, 255, 255)
        pygame.draw.line(
            self.screen, color, (20, HEIGHT - 20), (WIDTH - 20, HEIGHT - 20)
        )
        pygame.draw.circle(
            self.screen,
            color,
            (20 + round((WIDTH - 33) / len(anim) * anim_count), HEIGHT - 20),
            10,
            2,
        )
        pygame.display.update()
