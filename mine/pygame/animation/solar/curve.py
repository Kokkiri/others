import pygame
from utils import cosin, _next, selector
from math import sin, cos


class Curve:
    def __init__(self, screen):
        self.screen = screen

    def loop(self):
        pygame.init()
        clock = pygame.time.Clock()
        tick = 28
        dt = 1 / tick
        scene = "2"
        option = "6"
        count = 0
        lastTime = 0
        interval = 0.15
        x_angle = 200
        dot_range = 360
        x_fragment = 180
        y_angle = 200
        dot_range = 360
        y_fragment = 180
        run = True
        while run:
            clock.tick(tick)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        return "quit"
                    if event.key == pygame.K_LEFT and option == "6":
                        return _next(scene, 3, -1)
                    if event.key == pygame.K_RIGHT and option == "6":
                        return _next(scene, 3, 1)
            keys = pygame.key.get_pressed()
            # chose factor
            if keys[pygame.K_UP] and (count > lastTime + interval):
                lastTime = count
                option = _next(option, 6, 1)
            if keys[pygame.K_DOWN] and (count > lastTime + interval):
                lastTime = count
                option = _next(option, 6, -1)

            ###############    COS    ###############
            # chose x_angle
            if keys[pygame.K_LEFT] and option == "5":
                x_angle -= 1
            if keys[pygame.K_RIGHT] and option == "5":
                x_angle += 1

            # chose x_fragment
            if keys[pygame.K_LEFT] and option == "4":
                if x_fragment == 1:
                    x_fragment = 1
                else:
                    x_fragment -= 1
            if keys[pygame.K_RIGHT] and option == "4":
                if x_fragment == 360:
                    x_fragment = 360
                else:
                    x_fragment += 1
            #########################################

            ###############    SIN    ###############
            # chose angle
            if keys[pygame.K_LEFT] and option == "3":
                y_angle -= 1
            if keys[pygame.K_RIGHT] and option == "3":
                y_angle += 1

            # chose y_fragment
            if keys[pygame.K_LEFT] and option == "2":
                if y_fragment == 1:
                    y_fragment = 1
                else:
                    y_fragment -= 1
            if keys[pygame.K_RIGHT] and option == "2":
                if y_fragment == 360:
                    y_fragment = 360
                else:
                    y_fragment += 1
            #########################################

            # chose dot_range
            if keys[pygame.K_LEFT] and option == "1":
                if dot_range == 0:
                    dot_range = 0
                else:
                    dot_range -= 1
            if keys[pygame.K_RIGHT] and option == "1":
                dot_range += 1

            count += dt

            self.draw(
                count, option, x_angle, dot_range, x_fragment, y_angle, y_fragment
            )

    def draw(self, count, option, x_angle, dot_range, x_fragment, y_angle, y_fragment):

        ################    INIT    ###################
        screen_x_center = self.screen.get_width() / 2
        screen_y_center = self.screen.get_height() / 2
        ###############################################

        #############    DRAW CURVE    ################
        curve_x = [
            cosin(screen_x_center, x_angle, cos, n, x_fragment)
            for n in range(dot_range)
        ]
        curve_y = [
            cosin(screen_y_center, y_angle, sin, n, y_fragment)
            for n in range(dot_range)
        ]
        curve_coordinate = [(curve_x[n], curve_y[n]) for n in range(len(curve_x))]
        ###############################################

        #############    DRAW ELEMENTS    #############
        self.screen.fill((0, 0, 0))
        selector(self.screen, "6", option, "< CHANGE SCENE >", (10, 10))
        selector(self.screen, "5", option, f"X_ANGLE       < {x_angle} >", (10, 30))
        selector(self.screen, "4", option, f"X_FRAGMENT  < {x_fragment} >", (10, 50))
        selector(self.screen, "3", option, f"Y_ANGLE       < {y_angle} >", (10, 70))
        selector(self.screen, "2", option, f"Y_FRAGMENT  < {y_fragment} >", (10, 90))
        selector(self.screen, "1", option, f"DOT_RANGE    < {dot_range} >", (10, 110))

        for n in curve_coordinate:
            self.screen.set_at(n, (150, 150, 150))
        pygame.display.update()
        ###############################################
