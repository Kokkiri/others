import os
import pygame
from env import WIDTH, HEIGHT
from solar import Solar
from curve import Curve
from patio import Patio

################### INIT ##################
os.environ["SDL_VIDEO_CENTERED"] = "1"
screen = pygame.display.set_mode((WIDTH, HEIGHT))
###########################################


############	SCENE	########
def one():
    return Solar(screen).loop()


def two():
    return Curve(screen).loop()


def three():
    return Patio(screen).loop()


################################


def main_loop():
    action = {
        "1": one,
        "2": two,
        "3": three,
    }
    state = "1"

    while True:
        state = action[state]()
        if state == "quit":
            break

        pygame.QUIT


if __name__ == "__main__":
    main_loop()
