import pygame
from math import pi, cos, sin, acos, asin


# go to the next element of an array (ex: next scene or option)
def _next(state, options_num, num):
    t = [str(i) for i in range(1, options_num + 1)]
    for i in range(len(t)):
        if t[i] == state:
            return t[(i + num) % len(t)]


def cosin(center, radius, cosin, angle, fragment=1):
    return round(center + radius * cosin(angle * pi / fragment))


def countangle(angle):
    text = 0
    x = cos(pi * angle)
    y = sin(pi * angle)
    if y < 0:
        text = round(acos(x) * 180 / pi) + round(acos(-x) * 360 / pi)
    else:
        text = round(acos(x) * 180 / pi)
    return str(text)


def selector(screen, _id, option, text, pos):
    color = (100, 100, 100)
    if option == _id:
        color = (255, 255, 255)
    arial = pygame.font.Font("fonts/munro.ttf", 20)
    textSurface = arial.render(text, True, color, (0, 0, 0))
    screen.blit(textSurface, pos)
