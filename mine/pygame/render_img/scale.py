import pygame

pygame.init()

screen = pygame.display.set_mode((190, 190))

fio = pygame.image.load("img/fio_stand_0.png")
picture = pygame.transform.scale(fio, (150, 150))

# positionnement de l'image
rect = picture.get_rect()
rect = rect.move((20, 20))


def drawScreen():
    screen.fill((255, 255, 255))
    screen.blit(picture, rect)
    pygame.display.update()


run = True
while run:

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False

    drawScreen()

pygame.quit()
