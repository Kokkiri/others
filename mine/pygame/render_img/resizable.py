import os
import pygame
from pygame.locals import *

pygame.init()
screen = pygame.display.set_mode((500, 500), HWSURFACE | DOUBLEBUF | RESIZABLE)
pic = pygame.image.load("img/fio_stand_0.png")
screen.blit(pygame.transform.scale(pic, (500, 500)), (0, 0))
pygame.display.flip()

run = True
while run:
    # Je sais pas à quoi ça sert:
    # pygame.event.pump()
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False

        elif event.type == VIDEORESIZE:
            screen = pygame.display.set_mode(
                event.dict["size"], HWSURFACE | DOUBLEBUF | RESIZABLE
            )
            screen.blit(pygame.transform.scale(pic, event.dict["size"]), (0, 0))
            pygame.display.flip()
