import pygame


pygame.init()
print("press K_UP for scale")
print("press K_DOWN for smoothscale")
print("press K_LEFT for original sprite")
print("press K_RIGHT for flip")
print("press K_q to quit")


screen = pygame.display.set_mode((190, 190))

fio = pygame.image.load("img/fio_stand_0.png")
picture = fio


def drawScreen():
    rect = picture.get_rect()
    # positionnement de l'image
    rect = rect.move((20, 20))
    screen.fill((255, 255, 255))
    screen.blit(picture, rect)
    pygame.display.update()


run = True
while run:

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
            if event.key == pygame.K_DOWN:
                picture = pygame.transform.smoothscale(fio, (150, 150))
            if event.key == pygame.K_UP:
                picture = pygame.transform.scale(fio, (150, 150))
            if event.key == pygame.K_LEFT:
                picture = pygame.transform.flip(fio, 10, 0)
            elif event.key == pygame.K_RIGHT:
                picture = fio

    drawScreen()

pygame.quit()
