import pygame
from pathlib import Path
import glob

pygame.init()
screen = pygame.display.set_mode((200, 200))

CURRENT_PATH = Path(__file__).parent
PATH = CURRENT_PATH / "img"

anim = []
path = sorted(glob.glob(str(PATH / "*fio_stand_*.png")))
for p in path:
    anim.append(pygame.image.load(p))

clock = pygame.time.Clock()
count = 0


def drawScreen():
    screen.fill((100, 150, 200))
    images = pygame.transform.scale(anim[count], (200, 200))
    images_rect = images.get_rect()
    screen.blit(images, images_rect)
    pygame.display.update()


run = True
while run:
    clock.tick(5)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
    if count + 1 >= len(anim):
        count = 0
    count += 1
    drawScreen()

pygame.quit()
