import pygame

pygame.init()

screen = pygame.display.set_mode((200, 200))
clock = pygame.time.Clock()
red = (255, 0, 0)
blue = (0, 0, 255)
green = (0, 255, 0)
yellow = (250, 250, 0)

r = 0
g = 0
b = 0
current_color = (r, g, b)


def drawScreen(color):
    screen.fill(color)
    # pygame.draw.circle(screen, (255,255,255), (100,100), 50 )
    pygame.display.update()


# first arg = r,g or b; second arg = old_color[i]; third arg = current_color[i]
def color_transition(rgb, current_color_i):
    if rgb != current_color_i:
        if rgb < current_color_i:
            rgb += 5
        if rgb > current_color_i:
            rgb -= 5

    if rgb < 0:
        rgb = 0
    if rgb > 255:
        rgb = 255

    return rgb


run = True
while run:
    clock.tick(120)

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                run = False
            if event.key == pygame.K_LEFT:
                change_color = True
                current_color = blue
            if event.key == pygame.K_RIGHT:
                change_color = True
                current_color = red
            if event.key == pygame.K_UP:
                change_color = True
                current_color = green
            if event.key == pygame.K_DOWN:
                change_color = True
                current_color = yellow

    r = color_transition(r, current_color[0])
    g = color_transition(g, current_color[1])
    b = color_transition(b, current_color[2])

    drawScreen((r, g, b))

pygame.quit()
