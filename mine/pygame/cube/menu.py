import pygame, random
from base import Base
from animation import anim_start, anim_start2
from hud import Hud


class Menu(Base):
    """docstring for Menu."""

    hud = Hud()

    def __init__(self, clock):
        super(object, self).__init__()
        self.anim = anim_start
        self.clock = clock
        self.count = -1

    def main(self):
        launch = False
        launch_2 = False
        while True:
            self.clock.tick(15)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        return "quit"
                    if event.key == pygame.K_SPACE:
                        self.screen.setmode()
                    if event.key == pygame.K_LEFT:
                        launch = True
                    if event.key == pygame.K_RIGHT:
                        return "quit"

            if launch and self.count == len(self.anim) - 1:
                if launch_2:
                    return "navigation"
                else:
                    self.count = -1
                    self.anim = anim_start2
                    launch_2 = True

            if self.count + 1 >= len(self.anim):
                self.count = -1
            self.count += 1

            self.draw()

    def draw(self):
        sc = self.screen.screen
        sc_x = self.screen.screen_x
        sc_y = self.screen.screen_y
        text_facet = "Menu"
        text_button_left = f"< start "
        text_button_right = f" quit >"

        sc.fill(((0, 0, 0)))

        self.animate(self.count, self.anim, sc_x, sc_y)
        self.hud.display_all_text(
            text_facet, text_button_left, text_button_right, sc_x, sc_y
        )

        pygame.display.update()
