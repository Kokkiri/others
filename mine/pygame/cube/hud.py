import pygame
from base import Base
from screen import Screen


class Hud(Base):
    """interface about prayer status"""

    screen = Screen()
    sc = screen.screen
    sc_x = screen.screen_x
    sc_y = screen.screen_y

    def __init__(self):
        super(object, self).__init__()
        self.gauge_y = -450
        self.len_anim_start = 1
        self.len_anim_end = 1
        self.next_state = ""
        self.len_gauge = 1

    def display_all_text(
        self, text_facet, text_button_left, text_button_right, sc_x, sc_y
    ):
        self.display_text(text_facet, sc_x / 2, sc_y * 10 / 100)
        self.display_text(text_button_left, sc_x * 5 / 100, sc_y - sc_y * 5 / 100)
        self.display_text(
            text_button_right, sc_x - 5 * sc_x / 100, sc_y - sc_y * 5 / 100
        )

    def display_gauge(self):
        pygame.draw.rect(self.sc, (255, 255, 255), (10, self.sc_y - 100, 10, -450))
        pygame.draw.rect(self.sc, (0, 0, 0), (10, self.sc_y - 100, 10, self.gauge_y))

    def gauge_decrease(self, lenght):
        brick = 450 / lenght
        if self.gauge_y >= 0:
            self.gauge_y = -brick
        self.gauge_y += brick
