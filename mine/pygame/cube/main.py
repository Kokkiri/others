import pygame
from navigation import Navigation
from menu import Menu


def main_loop():
    pygame.init()

    clock = pygame.time.Clock()

    menu = Menu(clock)
    nav = Navigation(clock)

    action = {
        "menu": menu,
        "navigation": nav,
    }
    state = "menu"

    while True:
        state = action[state].main()
        if state == "quit":
            break

    pygame.quit()


if __name__ == "__main__":
    main_loop()
