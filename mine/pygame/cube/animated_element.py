import pygame


class Animated_element(object):
    """allow dynamic element in the sky like cloud, sun and moon"""

    def __init__(self):
        super(object, self).__init__()
        self.count = 0
        self.element_passes_x = 0
        self.element_passes_y = 0
        self.last_element_y = 0
        self.current_element_y = 1
        self.increment = 0
        self.decrement = 40

    def get_last_and_current_pos_element_y(self):
        self.last_element_y = self.current_element_y
        self.current_element_y = self.element_passes_y

    def scrolling_x(
        self,
        road_is_turning,
        turn_left,
        last_road_is_turning,
        last_road_is_turning_left,
    ):
        if road_is_turning:
            self.decrement = 40
            self.increment += 5
            if self.increment > 40:
                self.increment = 40
            if turn_left:
                self.element_passes_x += self.increment
            else:
                self.element_passes_x -= self.increment
        else:
            self.increment = 0
            self.decrement -= 5
            if self.decrement < 0:
                self.decrement = 0
            if last_road_is_turning:
                if last_road_is_turning_left:
                    self.element_passes_x += self.decrement
                else:
                    self.element_passes_x -= self.decrement
        return self.element_passes_x

    def set_count(self, anim):
        if self.count + 1 >= len(anim):
            self.count = -1
        self.count += 1

    def scrolling_y(self, current_day_night):
        if self.element_passes_y != -600 and self.element_passes_y != 600:
            if self.last_element_y > self.current_element_y:
                self.element_passes_y -= 20
            else:
                self.element_passes_y += 20
        elif current_day_night:
            self.element_passes_y += 20
        else:
            self.element_passes_y -= 20
        return self.element_passes_y

    def no_way_out_from_screen_x(self, pos_element_x, sc_x):
        if pos_element_x < -sc_x:
            self.element_passes_x = sc_x
        if pos_element_x > sc_x:
            self.element_passes_x = -sc_x

    def no_way_out_from_screen_y(self, pos_element_y, sc_y):
        if pos_element_y < -sc_y:
            self.element_passes_y = -sc_y
        if pos_element_y > sc_y:
            self.element_passes_y = sc_y - 300


class Sky_color(object):
    """allow transition color of the sky between animations"""

    def __init__(self):
        super(object, self).__init__()

    # rgb means r,g or b
    def color_transition(self, rgb, current_color_i):
        if rgb != current_color_i:
            if rgb < current_color_i:
                rgb += 10
            if rgb > current_color_i:
                rgb -= 10

        if rgb < 0:
            rgb = 0
        if rgb > 255:
            rgb = 255

        return rgb
