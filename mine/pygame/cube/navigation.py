import pygame, json
from base import Base
from animation import *
from pathlib import Path
from animated_element import *
from hud import Hud


with open(CURRENT_PATH / "nav-animation.json", "r") as f:
    data = json.load(f)

anim = {
    "anim_1": anim_1,
    "anim_1_2": anim_1_2,
    "anim_2": anim_2,
    "anim_2_3": anim_2_3,
    "anim_3": anim_3,
    "anim_3_6": anim_3_6,
    "anim_6": anim_6,
    "anim_6_2": anim_6_2,
    "anim_2_bis_10_bis": anim_2_bis_10_bis,
    "anim_10_12": anim_10_12,
    "anim_12": anim_12,
    "anim_12_23": anim_12_23,
    "anim_23": anim_23,
    "anim_23_47": anim_23_47,
    "anim_47": anim_47,
}


class Navigation(Base):
    """docstring for Navigation."""

    cloud = Animated_element()
    sun = Animated_element()
    moon = Animated_element()
    road = Animated_element()
    eva = Animated_element()
    sky = Sky_color()
    hud = Hud()

    def __init__(self, clock):
        super(object, self).__init__()
        self.clock = clock
        self.state = "1"
        self.action = data
        self.anim = anim["anim_1"]
        self.last_day_night = False
        self.current_day_night = True
        self.last_direction = False
        self.current_direction = False
        self.last_road_is_turning_left = False
        self.tick = 15

    def main(self):
        len_gauge = 1
        r = 200
        g = 100
        b = 100
        current_color = (200, 100, 100)
        is_open_one = False
        is_open_two = False
        while True:
            self.clock.tick(self.tick)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        return "quit"
                    if event.key == pygame.K_LEFT:
                        is_open_one = True
                        is_open_two = False
                    if event.key == pygame.K_RIGHT:
                        is_open_two = True
                        is_open_one = False
                    if event.key == pygame.K_UP:
                        self.tick += 2
                    if event.key == pygame.K_DOWN:
                        self.tick -= 2

            # if animation is not of type loop, it goes automaticaly to next animation
            if (
                self.action[self.state]["loop"] == False
                and self.road.count == len(self.anim) - 1
            ):
                # reset count and sky's color and go to the next screen
                self.state, self.road.count = (
                    self.action[self.state]["default_next_screen"],
                    -1,
                )
                current_color = self.action[self.state]["color"]
                # to be notice of change of day night cycle
                self.last_day_night = self.current_day_night
                self.current_day_night = self.action[self.state]["sun"]
                # to be notice of change of road direction (turn or not)
                self.last_direction = self.current_direction
                self.current_direction = self.action[self.state]["is_turning"]
                # to be notice of last direction of the road (left or right)
                if self.action[self.state]["is_turning"]:
                    self.last_road_is_turning_left = self.action[self.state][
                        "turn_left"
                    ]
                if self.action[self.state]["type"] == "start":
                    next_state = self.action[self.state]["default_next_screen"]
                    len_anim_start = len(anim[self.action[self.state]["animation"]])
                    len_anim_end = len(anim[self.action[next_state]["animation"]])
                    len_gauge = len_anim_start + len_anim_end
                    self.hud.gauge_y = -450 - 450 / len_gauge

            # if animation is of type loop, waiting for user input
            elif self.action[self.state]["loop"]:
                if is_open_one == True and self.count == len(self.anim) - 1:
                    self.state, self.road.count = (
                        self.action[self.state]["left_next_screen"],
                        -1,
                    )
                    is_open_one = False

                if is_open_two == True and self.count == len(self.anim) - 1:
                    self.state, self.road.count = (
                        self.action[self.state]["right_next_screen"],
                        -1,
                    )
                    is_open_two = Falsef

            # define current animation
            self.anim = anim[self.action[self.state]["animation"]]

            # COUNT #########################################
            self.road.set_count(self.anim)
            self.cloud.set_count(anim_cloud)
            self.eva.set_count(anim_evangelion)
            #################################################

            self.hud.gauge_decrease(len_gauge)

            # allow anim of sun and moon until they reach top or buttom screen
            # independantly of day_night changing
            self.sun.get_last_and_current_pos_element_y()
            self.moon.get_last_and_current_pos_element_y()

            r = self.sky.color_transition(r, current_color[0])
            g = self.sky.color_transition(g, current_color[1])
            b = self.sky.color_transition(b, current_color[2])

            self.draw((r, g, b))

    def draw(self, color):
        sc = self.screen.screen
        sc_x = self.screen.screen_x
        sc_y = self.screen.screen_y
        text_button_left = f'< {self.action[self.state]["text_button_left"]} '
        text_button_right = f' {self.action[self.state]["text_button_right"]} >'
        text_facet = self.action[self.state]["text_facet"]

        # SKY ########################################
        sc.fill(color)
        ##############################################

        # SUN ########################################
        pos_sun = self.animate(
            0,
            anim_sun,
            sc_x,
            sc_y,
            pos_x=self.sun.scrolling_x(
                self.action[self.state]["is_turning"],
                self.action[self.state]["turn_left"],
                self.last_direction,
                self.last_road_is_turning_left,
            ),
            pos_y=self.sun.scrolling_y(self.current_day_night),
        )
        pos_sun_x = pos_sun[0]
        pos_sun_y = pos_sun[1]
        self.sun.no_way_out_from_screen_x(pos_sun_x, sc_x)
        self.sun.no_way_out_from_screen_y(pos_sun_y, sc_y)
        ##############################################

        # MOON #######################################
        # pos_moon = self.animate(
        #     0,
        #     anim_moon,
        #     sc_x,
        #     sc_y,
        #     pos_x=self.moon.scrolling_x(
        #         self.action[self.state]["is_turning"],
        #         self.action[self.state]["turn_left"],
        #         self.last_direction,
        #         self.last_road_is_turning_left
        #     ),
        #     pos_y=self.moon.scrolling_y(
        #         self.current_day_night
        #     )
        # )
        # pos_moon_x = pos_moon[0]
        # pos_moon_y = pos_moon[1]
        # self.moon.no_way_out_from_screen_x(pos_moon_x, sc_x)
        # self.moon.no_way_out_from_screen_y(pos_moon_y, sc_y)
        #################################################

        # CLOUD #########################################
        pos_cloud_x = self.animate(
            self.cloud.count,
            anim_cloud,
            sc_x,
            sc_y,
            pos_x=self.cloud.scrolling_x(
                self.action[self.state]["is_turning"],
                self.action[self.state]["turn_left"],
                self.last_direction,
                self.last_road_is_turning_left,
            ),
        )[0]
        self.cloud.no_way_out_from_screen_x(pos_cloud_x, sc_x)
        self.cloud.element_passes_x += 2
        #################################################

        # ROAD ##########################################
        self.animate(self.road.count, self.anim, sc_x, sc_y)
        #################################################

        # HUD ###########################################
        self.hud.display_all_text(
            text_facet, text_button_left, text_button_right, sc_x, sc_y
        )
        self.hud.display_gauge()
        #################################################

        # ANIM EVANGELION ###############################
        # self.animate(self.eva.count, anim_evangelion, sc_x, 345)
        #################################################

        pygame.display.update()
