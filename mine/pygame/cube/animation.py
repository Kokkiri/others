import pygame
from pathlib import Path
import glob


CURRENT_PATH = Path(__file__).parent
IMG_PATH = CURRENT_PATH / "img"


anim_start = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "start_*.png")))
]

anim_start2 = [
    pygame.image.load(path)
    for path in sorted(glob.glob(str(IMG_PATH / "start2_*.png")))
]

anim_1 = [
    pygame.image.load(path)
    for i in range(5)
    for path in sorted(glob.glob(str(IMG_PATH / "1_0[1-3].png")))
]

anim_1_2 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "1_2_*.png")))
]

anim_2 = [
    pygame.image.load(path)
    for path in sorted(glob.glob(str(IMG_PATH / "2_[0-9][0-9].png")))
]

anim_2_3 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "2_3_*.png")))
]

anim_3 = [
    pygame.image.load(path)
    for i in range(5)
    for path in sorted(glob.glob(str(IMG_PATH / "3_0[1-3].png")))
]

anim_3_6 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "3_6_*.png")))
]

anim_6 = [
    pygame.image.load(path)
    for i in range(5)
    for path in sorted(glob.glob(str(IMG_PATH / "6_0*.png")))
]

anim_6_2 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "6_2_*.png")))
]

anim_2_bis_10_bis = [
    pygame.image.load(path)
    for i in range(2)
    for path in sorted(glob.glob(str(IMG_PATH / "2_bis_*.png")))
]

anim_10_12 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "10_12_*.png")))
]

anim_12 = [
    pygame.image.load(path)
    for i in range(5)
    for path in sorted(glob.glob(str(IMG_PATH / "12_0[1-3].png")))
]

anim_12_23 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "12_23_*.png")))
]

anim_23 = [
    pygame.image.load(path)
    for i in range(5)
    for path in sorted(glob.glob(str(IMG_PATH / "23_0[1-3].png")))
]

anim_23_47 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "23_47_*.png")))
]

anim_47 = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "47_*.png")))
]

anim_cloud = [
    pygame.image.load(path) for path in sorted(glob.glob(str(IMG_PATH / "cloud_*.png")))
]

anim_sun = [pygame.image.load(str(IMG_PATH / "sun.png"))]

anim_moon = [pygame.image.load(str(IMG_PATH / "moon.png"))]

anim_evangelion = [
    pygame.image.load(path)
    for path in sorted(glob.glob(str(IMG_PATH / "evangelion" / "*.png")))
]
