import pygame
from screen import Screen


class Base(object):
    """docstring for Base."""

    screen = Screen()
    sc = screen.screen
    pygame.mouse.set_visible(0)

    def __init__(self):
        super(object, self).__init__()

    def text_objects(self, text, font, color_text=(255, 255, 255), color_bg=(0, 0, 0)):
        textSurface = font.render(text, True, color_text, color_bg)
        return textSurface, textSurface.get_rect()

    def display_text(self, text, pos_x, pos_y, size=60):
        largeText = pygame.font.Font("font/m3x6.ttf", size)
        TextSurf, TextRect = self.text_objects(text, largeText)
        TextRect.center = (pos_x, pos_y)
        self.sc.blit(TextSurf, TextRect)

    def animate(self, count, animation, width, height, pos_x=0, pos_y=0):
        images = pygame.transform.scale(animation[count], (width, height))
        images_rect = images.get_rect()
        images_rect.topleft = (pos_x, pos_y)
        self.sc.blit(images, images_rect)
        return [pos_x, pos_y]
