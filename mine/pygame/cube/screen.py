import pygame


class Screen(object):
    """docstring for fullscreen."""

    def __init__(self):
        super(object, self).__init__()
        self.screen_x = 600
        self.screen_y = 600
        self.screen = pygame.display.set_mode([self.screen_x, self.screen_y])

    def setmode(self):
        if self.screen.get_flags() == 0:
            self.screen = pygame.display.set_mode(
                [self.screen_x, self.screen_y], pygame.FULLSCREEN
            )
            pygame.display.flip()
        else:
            self.screen = pygame.display.set_mode([self.screen_x, self.screen_y])
            pygame.display.flip()
