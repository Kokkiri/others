import sys

ma_list = {
    "a": "4",
    "4": "a",
    "b": "d",
    "d": "b",
    "e": "3",
    "3": "e",
    "i": "!",
    "x": "+",
    "y": "h",
    "h": "y",
    "m": "w",
    "w": "m",
    "n": "u",
    "u": "n",
    "g": "9",
    "9": "g",
    "q": "p",
    "p": "q",
    "s": "5",
    "5": "s",
    "o": "0",
    "0": "o",
    "_": "",
}


def generator(mdp):
    msg = ""
    for letter in mdp:
        try:
            if letter == letter.upper():
                msg += ma_list[letter.lower()].upper()
            else:
                msg += ma_list[letter.lower()]
        except KeyError:
            msg += letter
    print(msg[::-1])


generator(sys.argv[1])
