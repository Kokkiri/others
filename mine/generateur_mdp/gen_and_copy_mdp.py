import subprocess
import sys
import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, "generateur_mdp.py")

# ps = subprocess.Popen(['tree'], stdout=subprocess.PIPE)
# subprocess.run(['grep', 'generateur'], stdin=ps.stdout)

args = sys.argv[1]
ps = subprocess.Popen(["python3", filename, args], stdout=subprocess.PIPE)
subprocess.run(["xclip", "-selection", "clipboard"], stdin=ps.stdout)
