list = []

rep = int(
    input("rentrez un nombre. entrez un nombre inférieur ou égal à 0 pour sortir.")
)

while rep > 0:
    list.append(rep)
    rep = int(
        input("rentrez un nombre. entrez un nombre inférieur ou égal à 0 pour sortir.")
    )

r = [l for l in list if l > 100]

print(f"il y a {len(list)} nombres au total, dont {len(r)} était supérieur à 100")
