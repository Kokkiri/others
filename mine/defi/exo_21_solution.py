# -*- coding : utf8 -*-
"""
Jeu de dés (3).
exo numéro 21
"""

# Globale ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MAX = 8
NUMBER = 6

# Programme principal =========================================================
nbd = int(input(f"Nombre de dés [2 .. {MAX}] :"))
while not (nbd >= 2 and nbd <= MAX):
    nbd = int(input(f"Nombre de dés [2 .. {MAX}], s.v.p. :"))

# s la somme à obtenir
s = int(input(f"Entrez un entier [{nbd} .. {NUMBER * nbd}] :"))
while not (s >= nbd and s <= NUMBER * nbd):
    s = int(input(f"Entrez un entier [{nbd} .. {NUMBER * nbd}], s.v.p. :"))

# initialize sequence of dices
seq = [1] * nbd
# initialize count of solution and index of dices
solution_number, dice_index = 0, 0

if s == nbd or s == NUMBER * nbd:
    solution_number = 1  # 1 seule solution
else:
    # while index is lower than number of dices
    while dice_index < nbd:
        # calculate the sum of dices
        _sum = sum([seq[k] for k in range(nbd)])
        # if sum is equal to sum we want
        if _sum == s:
            # increase the count
            solution_number += 1
            # show result
            print(seq)
        # if sum is equal to 6 * number of dices
        if _sum == NUMBER * nbd:
            # break
            break
        ############## ITERATE ON NUMBER AND INDEX BOTH ################
        # initialize index of dice to 0
        dice_index = 0
        # if dice number is lower than 6
        if seq[dice_index] < NUMBER:
            # dice number increase by 1
            seq[dice_index] += 1
        # else
        else:
            # while dice number is equal to 6
            while seq[dice_index] == NUMBER:
                # dice number is equal to 1
                seq[dice_index] = 1
                # go to next dice
                dice_index += 1
            # dice number increase by 1
            seq[dice_index] += 1
################################################################

print(
    f"Il y a \033[1;32m{solution_number}\033[0;0m façons de faire \033[1;32m{s}\033[0;0m avec \033[1;32m{nbd}\033[0;0m dés."
)
