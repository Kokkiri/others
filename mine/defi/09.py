poule = int(input("nombre de poules tuées : "))
chien = int(input("nombre de chiens tuées : "))
vache = int(input("nombre de vaches tuées : "))
ami = int(input("nombre d'amis tuées : "))


def amende(poule, chien, vache, ami):
    total = (poule + chien * 3 + vache * 5 + ami * 10) / 100
    res = int(total) * 200
    if res > 1:
        print("le chasseur doit payer {} euros.".format(res))
    else:
        print("le chasseur n'a rien à payer.")


amende(poule, chien, vache, ami)
