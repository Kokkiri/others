source: https://python.developpez.com/cours/apprendre-python-3/?page=exercices-corriges

## 01

Écrire un programme qui, à partir de la saisie d'un rayon et d'une hauteur, calcule le volume d'un cône droit.

## 02

Une boucle `while :` entrez un prix HT (entrez o pour terminer) et affichez sa valeur TTC.

## 03

Une autre boucle `while :` calculez la somme d'une suite de nombres positifs ou nuls. Comptez combien il y avait de données et combien étaient supérieures à 100.
Entrer un nombre inférieur ou égal à 0 indique la fin de la suite.

## 04

L'utilisateur donne un entier positif n et le programme affiche `PAIR` s'il est divisible par 2, `IMPAIR` sinon.

## 05

L'utilisateur donne un entier positif et le programme annonce combien de fois de suite cet entier est divisible par 2.

## 06

L'utilisateur donne un entier supérieur à 1 et le programme affiche, s'il y en a, tous ses diviseurs propres sans répétition ainsi que leur nombre. S'il n'y en a pas, il indique qu'il est premier. Par exemple :

    Entrez un entier strictement positif : 12
    Diviseurs propres sans répétition de 12 : 2 3 4 6 (soit 4 diviseurs propres)
    Entrez un entier strictement positif : 13
    Diviseurs propres sans répétition de 13 : aucun ! Il est premier

## 07

Écrire un programme qui approxime par défaut la valeur de la constante mathématique e, pour n

assez grand(56), en utilisant la formule :

        n
    e ≈ ∑   1/i!
        i=0

Pour cela, définissez la fonction factorielle et, dans votre programme principal, saisissez l'ordre n
et affichez l'approximation correspondante de e.

## 08

Un gardien de phare va aux toilettes cinq fois par jour. Or les WC sont au rez-de-chaussée…

Écrire une procédure (donc sans `return`) `hauteurParcourue` qui reçoit deux paramètres, le nombre de marches du phare et la hauteur de chaque marche (en cm), et qui affiche :
 
Sélectionnez

    Pour x marches de y cm, il parcourt z.zz m par semaine.

On n'oubliera pas :

* qu'une semaine comporte 7 jours ;
* qu'une fois en bas, le gardien doit remonter ;
* que le résultat est à exprimer en m.

## 09

Un permis de chasse à points remplace désormais le permis de chasse traditionnel. Chaque chasseur possède au départ un capital de 100 points. S'il tue une poule, il perd 1 point, 3 points pour un chien, 5 points pour une vache et 10 points pour un ami. Le permis coûte 200 euros.

Écrire une fonction `amende` qui reçoit le nombre de victimes du chasseur et qui renvoie la somme due.

Utilisez cette fonction dans un programme principal qui saisit le nombre de victimes et qui affiche la somme que le chasseur doit débourser.

## 10

Je suis ligoté sur les rails en gare d'Arras. Écrire un programme qui affiche un tableau me permettant de connaître l'heure à laquelle je serai déchiqueté par le train parti de la gare du Nord à 9 h (il y a 170 km entre la gare du Nord et Arras).

Le tableau prédira les différentes heures possibles pour toutes les vitesses de 100 km/h à 300 km/h, par pas de 10 km/h, les résultats étant arrondis à la minute inférieure.

* Écrire une procédure `tchacatchac` qui reçoit la vitesse du train et qui affiche l'heure du drame ;
* Écrire le programme principal qui affiche le tableau demandé.

## 11

Un programme principal saisit une chaîne d'ADN valide et une séquence d'ADN valide (valide signifie qu'elles ne sont pas vides et sont formées exclusivement d'une combinaison arbitraire de `"a"`, `"t"`, `"g"` ou `"c"`).

Écrire une fonction `valide` qui renvoie vrai si la saisie est valide, faux sinon.

Écrire une fonction `saisie` qui effectue une saisie valide et renvoie la valeur saisie sous forme d'une chaîne de caractères.

Écrire une fonction `proportion` qui reçoit deux arguments, la chaîne et la séquence et qui retourne la proportion de séquence dans la chaîne (c'est-à-dire son nombre d'occurrences).

Le programme principal appelle la fonction `saisie` pour la chaîne et pour la séquence et affiche le résultat.

Exemple d'affichage :
 
    chaîne : attgcaatggtggtacatg
    séquence : ca
    Il y a 10.53 % de "ca" dans votre chaîne.

## 12

Il s'agit d'écrire, d'une part, un programme principal et, d'autre part, une fonction utilisée dans le programme principal.

La fonction `listAleaInt(n, a, b)` retourne une liste de n entiers aléatoires dans `[a .. b]` en utilisant la fonction `randint(a, b)` du module `random`.

Dans le programme principal :

* construire la liste en appelant la fonction `listAleaInt()` ;
* calculer l'index de la case qui contient le minimum ;
* échangez le premier élément du tableau avec son minimum.

## 13

Comme précédemment, il s'agit d'écrire, d'une part, un programme principal et, d'autre part, une fonction utilisée dans le programme principal.

La fonction `listAleaFloat(n)` retourne une liste de n flottants aléatoires en utilisant la fonction `random()` du module `random`.

Dans le programme principal :

* saisir un entier n dans l'intervalle : `[2 .. 100]` ;
* construire la liste en appelant la fonction `listAleaFloat()` ;
* afficher l'amplitude du tableau (écart entre sa plus grande et sa plus petite valeur) ;
* afficher la moyenne du tableau.

## 14

Fonction renvoyant plusieurs valeurs sous forme d'un tuple.

Écrire une fonction `minMaxMoy()` qui reçoit une liste d'entiers et qui renvoie le minimum, le maximum et la moyenne de cette liste. Le programme principal appellera cette fonction avec la liste : `[10, 18, 14, 20, 12, 16]`.

## 15

Saisir un entier entre 1 et 3999 (pourquoi cette limitation ?). L'afficher en nombre romain.

## 16

Améliorer le script précédent en utilisant la fonction `zip().`

## 17

Un tableau contient n entiers (2 < *n* < 100) aléatoires tous compris entre 0 et 500. Vérifier qu'ils sont tous différents.

## 18

Proposer une autre version plus simple du problème précédent en comparant les longueurs des tableaux avant et après traitement ; le traitement consiste à utiliser une structure de données contenant des éléments uniques.

## 19

L'utilisateur donne un entier *n* entre 2 et 12, le programme donne le nombre de façons de faire *n* en lançant deux dés.

## 20

Même problème que le précédent, mais avec *n* entre 3 et 18 et trois dés.

## 21

Généralisation des deux questions précédentes. L'utilisateur saisit deux entrées, d'une part le nombre de dés, *nbd* (que l'on limitera pratiquement à 10) et, d'autre part la somme, *s*, comprise entre *nbd* et 6.*nbd*. Le programme calcule et affiche le nombre de façons de faire s avec les *nbd* dés.

## 22

Même problème que le précédent, mais codé récursivement.

## 23

Nombres parfaits et nombres chanceux.

* On appelle *nombre premier* tout entier naturel supérieur à 1 qui possède exactement deux diviseurs, lui-même et l'unité.
* On appelle *diviseur propre* de *n*, un diviseur quelconque de *n*, *n* exclu.
* Un entier naturel est dit *parfait* s'il est égal à la somme de tous ses diviseurs propres.
* Un entier *n* tel que : `(n+i+i2)` est premier pour tout *i* dans `[0,n−2]` est dit chanceux.

Écrire un module (`parfait_chanceux_m.py`) définissant quatre fonctions : `somDiv`, `estParfait`, `estPremier`, `estChanceux` et un `autotest`.

* La fonction `somDiv` retourne la somme des diviseurs propres de son argument.
* Les trois autres fonctions vérifient la propriété donnée par leur définition et retourne un booléen. Si par exemple la fonction `estPremier` vérifie que son argument est premier, elle retourne `True`, sinon elle retourne `False`.

La partie de test doit comporter quatre appels à la fonction `verif` permettant de tester `somDiv(12)`, `estParfait(6)`, `estPremier(31)` et `estChanceux(11)`.

    # !/usr/bin/env python
    # -*- coding : utf8 -*-
    """Module de vérification."""

    #==============================================================================
    # fichier : verif.py
    #
    # auteur : Bob Cordeau
    #==============================================================================

    # Import ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    from sys import exit

    # Définition de fonction ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def verif(entree, reference, precision=0, comment="") :
        """Vérifie que <entree> est égal à <référence>, à la précision près."""
        print("{} [{}, {}]".format(comment, entree, reference), end="")
        if abs(entree - reference) <= precision :
            print(" *** ok")
        else :
            print(" ### ko")

    # Auto-test ===================================================================
    if __name__=='__main__' :
        verif(abs(-6/2), 3, comment="\nTeste la fonction 'abs' :")

        pi = 3.142
        print("\npi = {}".format(pi))
        verif(pi, 3.14, 1e-2, comment="à 1e-2 :") # Ok
        verif(pi, 3.14, 1e-3, comment="à 1e-3 :") # Erreur

Puis écrire le programme principal (`parfait_chanceux.py`) qui comporte :

* l'initialisation de deux listes : `parfaits` et `chanceux` ;
* une boucle de parcours de l'intervalle `[2,1000]` incluant les tests nécessaires pour remplir ces listes ;
* enfin l'affichage de ces listes.

