#!/usr/bin/python3

# import    ||||||||||||||||||||
import termios, sys, tty
from colored import fg, bg, attr
import json
import random

# get vocabulary    ||||||||||||
with open("words.json") as file:
    data = json.load(file)

# mix card  ||||||||||||||||||||||||||||||||||||
new_set = []
for el in data:
    card = data.get(el)
    new_set.append(card)
mixed_set = random.sample(new_set, len(new_set))


# good and bad answers  ||||||||||||||||||||||||||||||||||||||
def good():
    print("%s%s%s  %s" % (fg(255), bg(46), attr(1), attr(0)))


def nogood(resp):
    print(
        "%s%s%s  %s" % (fg(255), bg(196), attr(1), attr(0)),
        f"la réponse était {resp}\n",
    )


# response choice   ||||||||||||||||||||||
def choice_list(tab, i, bin):
    t = []
    # choice number
    for j in range(4):
        t.append(random.choice(mixed_set))
    t.append(mixed_set[i])
    for el in t:
        tab.append(el[bin])


# just press a number, no need to confirm with key enter
def _getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(fd)
        # This number represents the length
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


def result(choice, getch, i, bin, res):
    if choice[int(getch) - 1] == mixed_set[i][bin]:
        good()
        res.append(1)
    else:
        nogood(mixed_set[i][bin])
        res.append(2)
    return res


def distribute_cards():
    res = []
    card_number = 10

    print(f"\n\tnombre de carte: {card_number}\n")
    print(f" Entrez le numéro correpondant à la réponse\n")

    for i in range(card_number):
        newlist = []
        tab = []
        bin = random.randint(0, 1)
        if bin == 0:
            choice_list(tab, i, 1)
        if bin == 1:
            choice_list(tab, i, 0)
        choice = random.sample(tab, len(tab))

        for el in choice:
            print(f"\t{choice.index(el) + 1} {el}")

        print(f"\n\t{mixed_set[i][bin]}\n")

        getch = _getch()

        try:
            float(getch)
        except ValueError:
            print("%s Valeur incorrect %s" % (fg(226), attr(0)))
            print()
            continue
        try:
            newlist.append(choice[int(getch) - 1])
        except IndexError:
            print(
                "%s sélectionnez un numéro parmi ceux proposés %s" % (fg(226), attr(0))
            )
            print()
            continue

        if bin == 0:
            toto = result(choice, getch, i, 1, res)

        else:
            toto = result(choice, getch, i, 0, res)

    print(toto.count(1), "bonne réponse")
    print(toto.count(2), "mauvaise réponse")
    ratio = int(toto.count(1) / card_number * 100)
    print(ratio, "% de réussite")


distribute_cards()
