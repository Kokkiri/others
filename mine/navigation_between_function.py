from functools import wraps
import time


def graceful_ctrlc(func):
    """
    Makes the decorated function exit with code 1 on CTRL+C.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeyboardInterrupt:
            exit(1)

    return wrapper


def one():
    print("this is one")
    time.sleep(1)
    return "two"


def two():
    print("this is two")
    time.sleep(1)
    return "one"


@graceful_ctrlc
def main_loop():

    action = {
        "one": one,
        "two": two,
    }
    state = "one"

    while True:
        state = action[state]()


if __name__ == "__main__":
    main_loop()
