import requests
from bs4 import BeautifulSoup

######################## WEB SCRAPING #########################

url = "https://cemantix.certitudes.org/"
html = requests.get(url).text
html_page = BeautifulSoup(html, "html.parser")
_id = html_page.find("input", {"id": "cemantix-guess"})

_id["value"] = "toto"
print(_id)


################################ DOWNLOAD ###################################

# download_url = 'https://www.linuxjobs.fr/categories/1/postes-programmeur/rss'
# req = requests.get(download_url)
# filename = req.url[download_url.rfind('/')+1:]
#
# with open(filename, 'wb') as f:
#     for chunk in req.iter_content(chunk_size=8192):
#         if chunk:
#             f.write(chunk)
