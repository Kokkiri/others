# http://www.pythonchallenge.com/pc/def/channel.html
import re
from zipfile import ZipFile, ZipInfo

comments = ""
with ZipFile("channel.zip", "r") as myzip:
    num = 90052
    while True:
        with open(f"channel/{num}.txt", "r") as file:
            read = file.read()
            comments += myzip.getinfo(f"{num}.txt").comment.decode()
            try:
                num = re.findall("(\d+)", read)[0]
            except:
                print(comments)
                break

# collect all comment () 46145.txt
# lib py chercher comment dans zip
