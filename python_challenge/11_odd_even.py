from PIL import Image

im = Image.open("odd_even_img/evil.jpg")
(w, h) = im.size

even = Image.new("RGB", (w // 2, h // 2))
odd = Image.new("RGB", (w // 2, h // 2))

for i in range(w):
    for j in range(h):
        p = im.getpixel((i, j))
        if (i + j) % 2 == 1:
            odd.putpixel((i // 2, j // 2), p)
        else:
            even.putpixel((i // 2, j // 2), p)
even.save("odd_even_img/even.jpg")
odd.save("odd_even_img/odd.jpg")
