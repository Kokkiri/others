# http://www.pythonchallenge.com/pc/def/oxygen.html

import urllib.request as req
from PIL import Image
import io

target = "http://pythonchallenge.com/pc/def/oxygen.png"
response = req.urlopen(target)
body = response.read()

f = io.BytesIO(body)

im = Image.open(f)
middle_height = im.size[1] // 2

px = im.load()

first_result = []

for x in range(0, im.size[0] - 21, 7):
    first_result.append(chr(px[x, middle_height][0]))

print("res 1 ", "".join(first_result))

message = [105, 110, 116, 101, 103, 114, 105, 116, 121]
second_result = []

for ord in message:
    second_result.append(chr(ord))

print("res 2 ", "".join(second_result))
