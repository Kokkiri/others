# http://www.pythonchallenge.com/pc/def/peak.html
import pickle
import os

# enregistrer la cible sous
# ouvrir le fichier (br est un argument pour bite read) puisque c'est ce qu'attend pickle.load

if os.path.getsize("05_pickle_source") > 0:

    f = pickle.load(open("05_pickle_source", "rb"))

for ligne in f:
    for el in ligne:
        print(el[0] * el[1], end="")
    print()
